#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class EduKitNucleus;
#pragma link C++ class EduKitNucleusInfo;
#pragma link C++ class EduKitNucleusInfoDB;
#pragma link C++ class EduKitNucleusSprite;
#pragma link C++ class EduKitLessonLoaderWindow;
#pragma link C++ class EduKitHelpWindow;
#pragma link C++ class EduKitExperiment;
#pragma link C++ class EduKitExperimentParamsFrame;
#pragma link C++ class EduKitLesson;
#pragma link C++ class EduKitInfoWidget;
#pragma link C++ class EduKitPlotWindow;
#pragma link C++ class EduKitPlotManager;
#pragma link C++ class EduKitNavBar;
#pragma link C++ class EduKitMainWindow;
#pragma link C++ class EduKitSimEngine;
#pragma link C++ class EduKitInputParamsWindow;
#pragma link C++ class EduKitMain;

#pragma link C++ global gEduKit;

#endif
