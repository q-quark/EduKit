/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitMain.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
 
#ifndef _EDUKIT_MAIN_H_
#define _EDUKIT_MAIN_H_

class TObject;
class TCanvas;

class EduKitNucleusInfoDB;
class EduKitMainWindow;
class EduKitExperiment;
class EduKitLesson;

class EduKitMain
{
public:
	static EduKitMain* Instance();
	virtual ~EduKitMain();

	TCanvas* GetCanvas() const; // set the active canvas to be the main canvas and return it
	EduKitExperiment* GetExperiment() const; // return current experiment
	EduKitLesson* GetCurrentLesson() const;

	const char* GetResourcePath() const;
	EduKitNucleusInfoDB* NucleaiDB() const;

	// ------------ Plots Methods ------------ 
	TObject* NewPlot(const char* className, const char* name, const char* title);
	void RemovePlot(const char* name);

	TObject* GetPlot(const char* name) const;

	void ShowPlot(const char* name);
	void HidePlot(const char* name);

	void ClearPlots();

	// ------------ Lessons Methods ------------ 
	void RegisterLesson(const char* name, EduKitLesson* lesson);

	void LoadLesson(const char* name); // name of directory (in $EDUKIT_PATH/lessons/)


	// ------------ Other Methods ------------ 
	void SetInfoText(const char* infoText); // set the text to be visible in the MainWindow's Info Text Widget

	void UpdateDraw();
private:
	EduKitMain();
	EduKitMain(const EduKitMain& ); // not implemented
	EduKitMain& operator=(const EduKitMain&); // not implemented

	EduKitMainWindow* fMainWindow;
	
public:
	ClassDef(EduKitMain,0)
};

R__EXTERN EduKitMain* gEduKit;
#endif