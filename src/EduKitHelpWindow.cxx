/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitHelpWindow.cxx) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#include <string>
#include <fstream>
#include <streambuf>

#include <TUrl.h>
#include <TSystem.h>
#include <TGHtml.h>

#include "EduKitMain.h"
#include "EduKitLesson.h"
#include "EduKitHelpWindow.h"


ClassImp(EduKitHelpWindow)
EduKitHelpWindow::EduKitHelpWindow(const TGWindow* parent, UInt_t width, UInt_t height)
 	: TGTransientFrame(gClient->GetRoot(), this, width, height, kTransientFrame),
 	fView(0),
 	fFileName()
{
	fView = new TGHtml(this, width, height);

	AddFrame(fView, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

	Layout();

	SetWindowName(Form("Lesson Walktrough - EduKit"));

	Connect("CloseWindow()", "EduKitHelpWindow", this, "Hide()");
}

EduKitHelpWindow::~EduKitHelpWindow()
{

}

Bool_t EduKitHelpWindow::LoadFile(const char* filePath)
{
	fFileName.Clear();
	fFileName = filePath;
	
	std::ifstream fileH(filePath);
	std::string fileContents;

	if(!fileH.is_open()) return kFALSE;

	fileH.seekg(0, std::ios::end);   
	fileContents.reserve(fileH.tellg());
	fileH.seekg(0, std::ios::beg);

	fileContents.assign((std::istreambuf_iterator<char>(fileH)),
	            std::istreambuf_iterator<char>());

	fileH.close();

	fView->Clear();

	TUrl url(gSystem->UnixPathName(filePath));
	TString fpath = url.GetUrl();
	fpath.ReplaceAll(gSystem->BaseName(fpath.Data()), "");
    fpath.ReplaceAll("file://", "");
	fView->SetBaseUri(fpath.Data());

	fView->ParseText(const_cast<char*>(fileContents.c_str()), 0);
	fView->Layout();
	
	return kTRUE;
}

void EduKitHelpWindow::Show()
{
	MapSubwindows();
	MapWindow();
	Layout();
}

void EduKitHelpWindow::Hide()
{
	DontCallClose();
	UnmapWindow();
}