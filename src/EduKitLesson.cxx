/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitLesson.cxx) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#include <TSystem.h>
#include <TEnv.h>

#include "EduKitLesson.h"

ClassImp(EduKitLesson)
EduKitLesson::EduKitLesson(const char* name)
	: TNamed(name, name),
	fDescription(),
	fExperimentClassName(),
	fExperimentFileName(),
	fManualFileName()
{

}

EduKitLesson::~EduKitLesson(){}

const char* EduKitLesson::GetDescription() const { return fDescription.Data(); }
const char* EduKitLesson::GetExperimentClass() const { return fExperimentClassName.Data(); }
const char* EduKitLesson::GetExperimentFile() const { return fExperimentFileName.Data(); }
const char* EduKitLesson::GetManual() const { return fManualFileName.Data(); }

const char* EduKitLesson::GetLessonDir() const
{
	return Form("%s/lessons/%s", gSystem->Getenv("EDUKIT_PATH"), fName.Data());
}

const char* EduKitLesson::GetResourceDir() const
{
	return Form("%s/res", GetLessonDir());
}

const char* EduKitLesson::GetDocDir() const
{
	return Form("%s/doc", GetLessonDir());
}

void EduKitLesson::SetDescription(const char* description) { fDescription = description; }
void EduKitLesson::SetExperimentClass(const char* experimentClassName) { fExperimentClassName = experimentClassName; }
void EduKitLesson::SetExperimentFile(const char* experimentFileName) { fExperimentFileName = experimentFileName; }
void EduKitLesson::SetManual(const char* manualFileName) { fManualFileName = manualFileName; }

Bool_t EduKitLesson::ReadFile(const char* fileName)
{
	TEnv env(fileName);
	if( env.ReadFile(fileName, kEnvLocal)!=0 ) return kFALSE;

	SetName( env.GetValue("Name", "") );
	SetTitle( env.GetValue("Title", "") );
	SetDescription( env.GetValue("Description", "") );
	SetExperimentClass( env.GetValue("ExperimentClass", "") );
	SetExperimentFile( env.GetValue("ExperimentFile", "") );
	SetManual( env.GetValue("Manual", "") );

	return kTRUE;
}

EduKitLesson* EduKitLesson::LoadFromFile(const char* fileName)
{
	EduKitLesson* lesson = new EduKitLesson("");
	if(!lesson->ReadFile(fileName)) {
		delete lesson; 
		lesson = 0;
	}

	return lesson;
}

void EduKitLesson::WriteFile(const char* fileName)
{
	//fileName = Form("%s/%s", GetLessonDir(), fileName);

	TEnv env(fileName);

	env.SetValue("Name", GetName());
	env.SetValue("Title", GetTitle());
	env.SetValue("Description", GetDescription());
	env.SetValue("ExperimentClass", GetExperimentClass());
	env.SetValue("ExperimentFile", GetExperimentFile());
	env.SetValue("Manual", GetManual());

	env.WriteFile(fileName);
}