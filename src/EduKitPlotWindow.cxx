/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitPlotWindow.cxx) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
 
#include <TObject.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TRootEmbeddedCanvas.h>


#include "EduKitPlotWindow.h"

ClassImp(EduKitPlotWindow);

EduKitPlotWindow::EduKitPlotWindow(const char* name, const TGWindow* parent, UInt_t width, UInt_t height)
	: TGTransientFrame(parent, 0, width, height, kVerticalFrame | kChildFrame | kTempFrame), 
	fCanvasWidget(0), 
	fPlotObject(0)
{
	SetCleanup(kDeepCleanup);
	SetName(name);

	fCanvasWidget = new TRootEmbeddedCanvas(0, this, width/2.0, height/2.0);
	
	TCanvas* curCanvas = fCanvasWidget->GetCanvas();
	curCanvas->cd(0);

	AddFrame(fCanvasWidget,  new TGLayoutHints(kLHintsExpandX | kLHintsExpandY) );

	Connect("CloseWindow()", "EduKitPlotWindow", this, "Hide()");

	MapSubwindows();
	MapWindow();
	Layout();
}

EduKitPlotWindow::~EduKitPlotWindow()
{

}

TCanvas* EduKitPlotWindow::GetCanvas() const
{
	return fCanvasWidget->GetCanvas();
}

void* EduKitPlotWindow::GetPlot() const
{
	return fPlotObject;
}

void EduKitPlotWindow::Show()
{
	fCanvasWidget->GetCanvas()->Modified();

	MapSubwindows();
	MapWindow();
	Layout();
}

void EduKitPlotWindow::Hide()
{
	DontCallClose();
	UnmapWindow();

	Emit("Hide()");
}