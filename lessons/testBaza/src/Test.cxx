#include <iostream>
#include "TMarker.h" 
#include "TCanvas.h"
#include "TGraph.h"
#include "EduKitMain.h"
#include "TMath.h"

#include "Test.h"

using namespace std;

ClassImp(TestBazaExperiment)

TestBazaExperiment::TestBazaExperiment() 
	: EduKitExperiment("test", "Acesta este titlul experimentului de TEST"),
	fParticle(0),
	fMarker(0)
{
cout << "Construim obiect TestBazaExperiment" << endl;
fParticle = new Particle;
SetDefaults();
}

TestBazaExperiment::~TestBazaExperiment()
{
delete fParticle;
}

void TestBazaExperiment::Init()
{
cout << "Construim obiect Init" << endl;
fParticle->x = 0.0;
fParticle->y = 0.0;

TCanvas* canvas = gEduKit->GetCanvas();
canvas->Range(-100, -100, 100, 100);
fMarker = new TMarker;
fMarker->SetMarkerStyle(2);
fMarker->SetMarkerSize(8);
fMarker->SetMarkerColor(2);

fMarker->SetX(fParticle->x);
fMarker->SetY(fParticle->y);

fMarker->Draw();

// Canvasul activ este plotul
TGraph* graph = (TGraph*)gEduKit->NewPlot("TGraph", "plot1", "Particle position");

graph->Set(GetStepEnd()-GetStepStart());
graph->Draw("A*");

gEduKit->UpdateDraw();


}

void TestBazaExperiment::NextStep(Int_t stepNumber)
{
cout << "Construim obiect NextStep: " << stepNumber << endl;
fParticle->x = 10.0*TMath::Cos(3.6*stepNumber);
fParticle->y = 10.0*TMath::Sin(3.6*stepNumber);

fMarker->SetX(fParticle->x);
fMarker->SetY(fParticle->y);

TGraph* g = (TGraph*)gEduKit->GetPlot("plot1");
g->SetPoint(stepNumber, fParticle->x, fParticle->y);

gEduKit->UpdateDraw();
}

void TestBazaExperiment::ClearAll()
{
cout << "Construim obiect ClearAll" << endl;

delete fMarker;
gEduKit->ClearPlots();
}

void TestBazaExperiment::SetDefaults()
{
cout << "Construim obiect SetDefaults" << endl;
fParticle->x = 0.0;
fParticle->y = 0.0;
}