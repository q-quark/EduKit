/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitExperiment.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "EduKitNucleusInfo.h"
#include "EduKitNucleusInfoDB.h"

ClassImp(EduKitNucleusInfoDB);

EduKitNucleusInfoDB::EduKitNucleusInfoDB(const char* filename)
{
   // printf("Opening db file %s!!!\n", filename);
    std::ifstream in_file;

    in_file.open(filename);

    if(!in_file.is_open())
    {
       // printf("\n-------------!!! Couldnt open file %s!!!\n\n", filename);
        return;
    }

    unsigned int line_count = 0;
    std::string line;
    double xA, xR;

    // printf("Reading db file %s!!!\n", filename);
    while (getline(in_file, line)) 
    {
        EduKitNucleusInfo* dbEntry = new EduKitNucleusInfo;
        dbEntry->id = line_count;
        dbEntry->Z = (atoi(line.substr(10,5).c_str()));
        dbEntry->A = (atoi(line.substr(15,5).c_str()));
        dbEntry->Symbol = (line.substr(20,3));
        dbEntry->MassExcess = strtod(line.substr(28,14).c_str(), NULL)/1000;
        xA = strtod(line.substr(96, 4).c_str(), NULL);
        xR = strtod(line.substr(100, 13).c_str(), NULL);
        dbEntry->Mass = 931.494095*xA + xR*931.494095/1.0e06;
        dbEntry->Charge = dbEntry->Z;

      //  if( line_count==61 ||  line_count==62) printf("Adding entry %d %s(%d,%d) mass:%lf\n", line_count, dbEntry->Symbol.c_str(), dbEntry->A, dbEntry->Z, dbEntry->Mass);
        fNucleusInfoList.push_back(dbEntry);
        line_count++;
    }

    in_file.close();
}


EduKitNucleusInfoDB::~EduKitNucleusInfoDB()
{
    for(unsigned int i=0; i< fNucleusInfoList.size(); i++)
    {
        EduKitNucleusInfo* dbEntry = fNucleusInfoList.at(i);
        delete dbEntry;
    }

    fNucleusInfoList.clear();

}


EduKitNucleusInfo* EduKitNucleusInfoDB::GetEntry(int a, int z) const
{
    EduKitNucleusInfo* dbEntry=0;
    for( unsigned int i=0; i < fNucleusInfoList.size(); i++)
    {
        dbEntry = fNucleusInfoList.at(i);

        if( (dbEntry->A == a) && (dbEntry->Z == z) )
        {
            return dbEntry;
        }
    }

    return 0;
}



std::string EduKitNucleusInfoDB::GetName(int a, int z) const
{
    EduKitNucleusInfo* dbEntry = GetEntry(a,z);

    std::string temp;

    if (dbEntry == 0)
        return temp;

    return dbEntry->Name;
}



std::string EduKitNucleusInfoDB::GetSymbol(int a, int z) const
{
    EduKitNucleusInfo* dbEntry = GetEntry(a,z);

    std::string temp;

    if (dbEntry == 0)
        return temp;

    return dbEntry->Symbol;
}



double EduKitNucleusInfoDB::GetMassInAMU(int a, int z) const
{
    EduKitNucleusInfo* dbEntry = GetEntry(a,z);

    if (dbEntry == 0)
        return 0.0;

    return (dbEntry->MassExcess/931.494 + dbEntry->A);
}


double EduKitNucleusInfoDB::GetMassInMeV(int a, int z) const
{
    EduKitNucleusInfo* dbEntry = GetEntry(a,z);

    if (dbEntry == 0)
        return 0.0;

    return (dbEntry->MassExcess/931.494 + dbEntry->A) * 931.494;
}


double EduKitNucleusInfoDB::GetMassExcessInMeV(int a, int z) const
{
    EduKitNucleusInfo* dbEntry = GetEntry(a,z);

    if (dbEntry == 0)
        return 0.0;

    return dbEntry->MassExcess;
}

    
double EduKitNucleusInfoDB::GetMassDefectInAMU(int a, int z) const
{
    EduKitNucleusInfo* dbEntryNeutron = GetEntry(1,0);
    EduKitNucleusInfo* dbEntryProton = GetEntry(1,1);
    EduKitNucleusInfo* dbEntry = GetEntry(a,z);

    if ((dbEntry == 0) || (dbEntryNeutron == 0) || (dbEntryProton == 0))
        return 0.0;

    return ( (dbEntryProton->MassExcess * dbEntry->Z) + 
        ( dbEntryNeutron->MassExcess * (dbEntry->A - dbEntry->Z) ) -
        (dbEntry->MassExcess/931.494 + dbEntry->A)*931.494 );
}


int EduKitNucleusInfoDB::GetId(int a, int z) const
{
    EduKitNucleusInfo* dbEntry = GetEntry(a,z);

    if (dbEntry == 0)
        return 0.0;

    return dbEntry->id;
}


double EduKitNucleusInfoDB::GetCharge(int a, int z) const
{
    EduKitNucleusInfo* dbEntry = GetEntry(a,z);

    if (dbEntry == 0)
        return 0.0;

    return dbEntry->Charge;
}