/*****************************************************************************************
 *                                                                                       
 *  This file (DecaysExperiment.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
 
#ifndef FISSION_CLASSES_H__
#define FISSION_CLASSES_H__ 

#include "EduKitExperiment.h"
#include "EduKitNucleusInfoDB.h"
#include "EduKitNucleus.h"

class FissionProbability;
class TMarker;
class TText;
class TArrow;
class TLatex;

class FissionExperiment : public EduKitExperiment
{
public:
	FissionExperiment();
	~FissionExperiment();

	void Init();
	void NextStep(Int_t stepNumber);
	void ClearAll();

	void SetDefaults();


protected:
	
FissionProbability* fU235FissionPROB;
EduKitNucleusInfoDB* fNucleusDB;
EduKitNucleus* fFragment1;
EduKitNucleus* fFragment2;

int fSimulatedA1;
int fSimulatedZ1;
	
TMarker* fMarkerU;
TMarker* fMarkerF1;
TMarker* fMarkerF2;
TMarker* fMarkerF1N1;
TMarker* fMarkerF1N2;
TMarker* fMarkerF1N3;
TMarker* fMarkerF2N1;
TMarker* fMarkerF2N2;
TMarker* fMarkerF2N3;
 
TText* fTextU;
TText* fTextF1;
TText* fTextF2;

float fXF1;
float fYF1;
float fXF2;
float fYF2;
float fXArF1;
float fYArF1;
float fXArF2;
float fYArF2;

int fMarkerSizeF1;
int fMarkerSizeF2;


TArrow* fArF1;
TArrow* fArF2;
TArrow* fArF1N1;
TArrow* fArF1N2;
TArrow* fArF1N3;
TArrow* fArF2N1;
TArrow*	fArF2N2;
TArrow*	fArF2N3;

	
TLatex* fReactionInfo;
TLatex* fFragment1Info;
TLatex* fFragment2Info;

public:
	ClassDef(FissionExperiment, 0)
};

#endif