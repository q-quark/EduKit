/*****************************************************************************************
 *                                                                                       
 *  This file (DecaysExperiment.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
 
#ifndef EXAMPLE_DECAYS_CLASSES_H__
#define EXAMPLE_DECAYS_CLASSES_H__ 

#include "EduKitExperiment.h"
#include "EduKitExperimentParamsFrame.h"

class TGLabel;
class TGTextEntry;
class TGDoubleHSlider;
class TMarker;
class DecaysExperimentParamsFrame;

struct RadAtom_t
{
	TMarker *sprite;
	bool decay; // false initialy
};

class DecaysExperiment : public EduKitExperiment
{
public:
	DecaysExperiment();
	~DecaysExperiment();

	void Init();
	void NextStep(Int_t stepNumber);
	void ClearAll();

	EduKitExperimentParamsFrame* GetParamsFrame();

	void SetDefaults();

	// Other methods
	Int_t GetAtomsNumber() const;
	Float_t GetAtomsHalfLife() const;

	void SetAtomsNumber(Int_t n);
	void SetAtomsHalfLife(Float_t hl);

protected:
	// Window Frame to setup the simulation variables
	DecaysExperimentParamsFrame* fParamsFrame;

	// Prefered Settings for this experiment
	Int_t fAtomsTotalNumber; 	// total Number of Atoms
	Int_t fAtomsRemained; // current Atoms remained undecayed
	Float_t fAtomHalfLife; // radioactive half-life
	Float_t fAtomDecayConstant; // radioactive decay constant
	RadAtom_t* fAtoms; // array of atoms

public:
	ClassDef(DecaysExperiment, 0)
};

class DecaysExperimentParamsFrame : public EduKitExperimentParamsFrame
{
public:
	DecaysExperimentParamsFrame();
	DecaysExperimentParamsFrame(DecaysExperiment* exp);
	~DecaysExperimentParamsFrame(){}

	void onAccepted();
	void onCanceled();
	void onDefaulted();	

protected:
	Float_t GetTimeStep() const;
	void LoadData(); // loads all data and set it to GUI objects

	DecaysExperiment* fExp; // parent experiment
	TGTextEntry* fEntryAtomHalfLife;
	TGTextEntry* fEntryAtomsTotalNumber;

	TGTextEntry* fEntrySimEventsNumber; // number of simulation events
public:
	ClassDef(DecaysExperimentParamsFrame, 0)
};
#endif