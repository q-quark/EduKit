#ifndef EXAMPLE_CLASSES_H__
#define EXAMPLE_CLASSES_H__ 

#include "EduKitExperiment.h"

struct Particle
{
	Float_t x;
	Float_t y;
};
class TestBazaExperiment : public EduKitExperiment
{
public:
	// MUST have at least one default constructor
	TestBazaExperiment();
	~TestBazaExperiment();

	void Init();
	void NextStep(Int_t stepNumber);
	void ClearAll();

	void SetDefaults();

	// Other methods

protected:
	// Here put your class variable members	
	Particle* fParticle;
	TMarker* fMarker;

public:
	ClassDef(TestBazaExperiment, 0)
};
#endif