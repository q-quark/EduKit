/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitExperiment.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#ifndef _EDUKIT_NUCLEUS_H_
#define _EDUKIT_NUCLEUS_H_

#include <Rtypes.h>

class EduKitNucleusInfo;

class EduKitNucleus
{
public:
	EduKitNucleus();
    EduKitNucleus(EduKitNucleusInfo* NucleusInfo, float x=0.0, float y=0.0, Double_t ke=0.0, Double_t p=0.0, float phi=0.0);

    ~EduKitNucleus();

    void SetX(float x);
    void SetY(float y);
    void SetInfo(EduKitNucleusInfo* NucleusInfo);
    void SetMomentum(Double_t p);
    void SetKEnergy(Double_t ke);
    void SetMomentumPhi(float phi);

    float GetX();
    float GetY();
    EduKitNucleusInfo* GetInfo();
    Double_t GetMomentum();
    Double_t GetKEnergy();
    float GetMomentumPhi();

protected:
	float fX;
	float fY;
	Double_t fKenergy;
	Double_t fMomentum;
	EduKitNucleusInfo* fNucleusInfo;
    float fPhi;

	ClassDef(EduKitNucleus,0)
};
 
#endif
