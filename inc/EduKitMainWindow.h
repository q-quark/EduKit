/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitMainWindow.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
 
#ifndef _EDUKIT_MAINWINDOW_H_
#define _EDUKIT_MAINWINDOW_H_

#include <TString.h>
#include <TGFrame.h>

class TGHtml;
class TGMenuBar;
class TGPopupMenu;
class TGPicturePool;
class TRootEmbeddedCanvas;

class EduKitNucleusInfoDB;

class EduKitNavBar;
class EduKitPlotManager;
class EduKitInfoWidget;

class EduKitLesson;
class EduKitExperiment;
class EduKitSimEngine;
class EduKitInputParamsWindow;
class EduKitHelpWindow;
class EduKitLessonLoaderWindow;

class EduKitMainWindow : public TGMainFrame
{
public:
	EduKitMainWindow(const TGWindow* p, int width, int height);
	virtual ~EduKitMainWindow();

	bool LoadLesson(EduKitLesson* lesson);
	void UnLoadCurrentLesson();

	TGPicturePool* GetPicturePool() const;

	// slots
	void onMenuActivated(Int_t id);
	void onMenuPlotActivated(Int_t id);

	void onShowSimSettings();
	void onSimStepEntering();
	void onSimStepFinished();

	void onAcceptedSimSettings();
	void onAcceptedLoadLesson(EduKitLesson* lesson);

	void ShowInfo();
	void ShowLessonHelp();
	//void ShowLessonsLoaderWindow();
	void ShowAboutDialog();

	void SimReset();
protected:
	friend class EduKitMain;

	void SetupMenu();

	// GUI elements
	TGPicturePool		*fPicturePool;
	TGMenuBar			*fMenuBar;
	TGPopupMenu			*fMenuFile;
	TGPopupMenu			*fMenuSimulation;
	TGPopupMenu			*fMenuPlots;
	TGPopupMenu			*fMenuHelp;

	TRootEmbeddedCanvas *fAnimationWidget; // for doing animations
	EduKitNavBar		*fNavBar; // for steering simulation
	EduKitInfoWidget	*fInfoWidget; // for displaying usefull information

	EduKitInputParamsWindow *fParamsWindow;
	
	EduKitHelpWindow	*fHtmlViewer;
	EduKitLessonLoaderWindow* fLessonLoaderWindow;

	// others
	EduKitPlotManager	*fPlotManager; // manages the plots
	EduKitSimEngine		*fSimEngine; // the simulation engine
	EduKitLesson		*fCurrentLesson;
	EduKitNucleusInfoDB *fNucleaiDB;
	
	TString	fBaseIncludePath;
public:

	ClassDef(EduKitMainWindow, 0)
};
#endif