/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitPlotManager.cxx) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
 
#include <TObject.h>
#include <TClass.h>
#include <TMap.h>
#include <TObjString.h>
#include <TCanvas.h>

#include <TGWindow.h>
#include <TGMenu.h>

#include "EduKitPlotWindow.h"
#include "EduKitPlotManager.h"

typedef EduKitObjectsMap::iterator EduKitObjectsMapIter;
typedef EduKitObjectsMap::const_iterator EduKitObjectsMapIterConst;
typedef EduKitObjectsIdsMap::iterator EduKitObjectsIdsMapIter;
typedef EduKitObjectsIdsMap::const_iterator EduKitObjectsIdsMapIterConst;

ClassImp(EduKitPlotManager);

EduKitPlotManager::EduKitPlotManager(TGPopupMenu* p)
	: fMenu(p),
	fPlotsMap(),
	fPlotsIdsMap(),
	fCurPlotIdsMax(0)
{

}

EduKitPlotManager::~EduKitPlotManager()
{
	Clear();
}

TObject* EduKitPlotManager::NewPlot(const char* className, const char* name, const char* title)
{
	if(fPlotsMap.count(name)>0) return 0; // duplicates are not allowed

	TClass* cl = TClass::GetClass(className);

	if(!cl) return 0;
	if(!cl->IsTObject() || !cl->InheritsFrom("TNamed")) return 0;

	EduKitPlotWindow* plotWin = new EduKitPlotWindow(name, gClient->GetRoot(), 400, 400);
	plotWin->SetWindowName(title);
	plotWin->fPlotObject = cl->New();
	plotWin->Connect("Hide()", "EduKitPlotManager", this, Form("HidePlot(=\"%s\")",name) );

	fPlotsMap[name] = plotWin;
	fPlotsIdsMap[plotWin] = fCurPlotIdsMax;

	// add an entry in the Plots Menu
	fMenu->AddEntry(title, fCurPlotIdsMax, (void*)name);
	fMenu->CheckEntry(fCurPlotIdsMax);

	fCurPlotIdsMax++;

	return (TObject*)plotWin->fPlotObject;
}

void EduKitPlotManager::RemovePlot(const char* name)
{
	TObject* objPtr = fPlotsMap[name];
	Int_t objId = fPlotsIdsMap[objPtr];

	if(!objPtr) return;

	// clears the maps.. but doesnt delete the Obj
	fPlotsMap.erase(name);
	fPlotsIdsMap.erase(objPtr);

	//  now, delete the Obj
	fMenu->DeleteEntry(objId);
}

TObject* EduKitPlotManager::GetPlot(const char* name) const
{
	EduKitPlotWindow* win =  GetPlotWindow(name);

	if(win){
		return (TObject*)win->GetPlot();
	}
	else {
		return 0;
	}
}

void EduKitPlotManager::ShowPlot(const char* name)
{
	EduKitPlotWindow* win = GetPlotWindow(name);
	
	if(!win) return;

	win->Show();

	fMenu->CheckEntry(fPlotsIdsMap[(TObject*)win]);
}

void EduKitPlotManager::HidePlot(const char* name)
{
	EduKitPlotWindow* win = GetPlotWindow(name);
	
	if(!win) return;
	
	if(win->IsMapped())	win->Hide();

	fMenu->UnCheckEntry(fPlotsIdsMap[(TObject*)win]);
}

void EduKitPlotManager::ShowAllPlots()
{
	TObject* plotWindowObj;
	for(EduKitObjectsMapIter iter=fPlotsMap.begin(); iter!=fPlotsMap.end(); iter++)
	{
		plotWindowObj = iter->second;
		if(plotWindowObj){
			((EduKitPlotWindow*)plotWindowObj)->Show();
			fMenu->CheckEntry(fPlotsIdsMap[plotWindowObj]);
		}
	}

}

void EduKitPlotManager::HideAllPlots()
{
	TObject* plotWindowObj;
	for(EduKitObjectsMapIter iter=fPlotsMap.begin(); iter!=fPlotsMap.end(); iter++)
	{
		plotWindowObj = iter->second;
		if(plotWindowObj){
			((EduKitPlotWindow*)plotWindowObj)->Hide();
			fMenu->UnCheckEntry(fPlotsIdsMap[plotWindowObj]);
		}
	}
}


void EduKitPlotManager::Clear()
{
	TObject* plotWindowObj;
	Int_t plotMenuId=-9999;
	for(EduKitObjectsMapIter iter=fPlotsMap.begin(); iter!=fPlotsMap.end(); iter++)
	{
		plotWindowObj = iter->second;
		if(plotWindowObj){
			plotMenuId = fPlotsIdsMap[plotWindowObj];
	
			fMenu->DeleteEntry(plotMenuId);
	
			((EduKitPlotWindow*)plotWindowObj)->CloseWindow();
		}
	}

	fPlotsMap.clear();
	fPlotsIdsMap.clear();

	fCurPlotIdsMax = 0;
}

EduKitPlotWindow* EduKitPlotManager::GetPlotWindow(const char* name) const
{	
	EduKitObjectsMapIterConst iter = fPlotsMap.find(name);

	if( iter != fPlotsMap.end() ){
		EduKitPlotWindow* win = dynamic_cast<EduKitPlotWindow*>(iter->second);
		return win;
	}
	else {
		return 0;
	}
}

void EduKitPlotManager::UpdateDraw()
{
	EduKitPlotWindow* plotWindowObj;
	for(EduKitObjectsMapIter iter=fPlotsMap.begin(); iter!=fPlotsMap.end(); iter++)
	{
		plotWindowObj = (EduKitPlotWindow*)iter->second;
		if(plotWindowObj->IsMapped()) {plotWindowObj->GetCanvas()->Modified(); plotWindowObj->GetCanvas()->Update();}
	}
}