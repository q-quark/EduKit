#include <iostream>
#include <cmath>
#include <fstream>

#include <TRandom.h>
#include <TMath.h>
#include <TPad.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TSystem.h>
#include <TString.h>
#include <TGLabel.h>
#include <TGDoubleSlider.h>
#include <TGTextEntry.h>
#include <TGLayout.h>
#include <TMarker.h>
#include <TArrow.h>
#include <TText.h>
#include <TLatex.h>

#include "EduKitNucleus.h"
#include "EduKitNucleusInfo.h"
#include "EduKitNucleusInfoDB.h"

#include "EduKitMain.h"
#include "EduKitLesson.h"
#include "alphadecay.h"

using namespace std;

	int A;
	int Z;
	int N;
	double EM;
	
	double Ealpha, Edaughter, Palpha, Pdaughter;
	double EalphaR, EdaughterR, PalphaR, PdaughterR;
	double m;
	double sigmaECinAlpha, sigmaECinDaughter, sigmaPAlpha, sigmaPDaughter;
	double c, d, f, T12;
	double rotAngle;

	const double u = 931.494; // transforma din u in mev
	const double mp = 1.00727646681*u;
	const double mn = 1.00866491588*u;
	const double malpha = 4.0318827654*u;
	const double EMalpha = 2.42491;
	
	double S; //energia de separare
	const double Salpha = 2.424911;


void RotateMarker(TMarker* marker, double angle)
{
    double x = marker->GetX();
    double y = marker->GetY();

    double xtemp = x;

    x = x*TMath::Cos(angle) - y * TMath::Sin(angle);
    y = xtemp*TMath::Sin(angle) + y * TMath::Cos(angle);

    marker->SetX(x);
    marker->SetY(y);
}

void RotateMarkerText(TText* text, double angle)
{
    double x = text->GetX();
    double y = text->GetY();

    double xtemp = x;

    x = x*TMath::Cos(angle) - y * TMath::Sin(angle);
    y = xtemp*TMath::Sin(angle) + y * TMath::Cos(angle);

    text->SetX(x);
    text->SetY(y);
}

double GradToRad(double grade)
{
    double rad = grade*TMath::Pi()/180.0;
    return rad;
}

ClassImp(alphadecay);
ClassImp(AlphaDecayExperimentParamsFrame);

alphadecay::alphadecay() 
	: EduKitExperiment("alphadecay", "Alpha Decay"),
	fMotherA(238),
	fMotherZ(92),
	fParticleMother(0),
	fParticleDaughter(0),
	fMotherMarker(0),
	fDaughterMarker(0),
	fAlphaMarker(0),
	fMotherText(0),
	fDaughterText(0),
	fAlphaText(0),
	fDaughterArrow(0),
	fAlphaArrow(0),
	fParamsFrame(0)
{
	printf("Creating settings frame... \n");
 	fParamsFrame = new AlphaDecayExperimentParamsFrame(this);
 
 	printf("Done.\n Diving Canvas...\n");
 	TPad* pad = gEduKit->GetCanvas();
    pad->Divide(2,2);

	SetDefaults();
}

alphadecay::~alphadecay()
{}

void alphadecay::Init()
{
	TPad* pad = gEduKit->GetCanvas();
    pad->cd(1);
    pad = (TCanvas*)pad->GetPad(1);
    gPad = pad;

    if(!pad) return;
    pad->Range(-100,-100, 100, 100); 

    EduKitNucleusInfoDB* NucleaiDB = gEduKit->NucleaiDB();
	fParticleMother   = new EduKitNucleus(NucleaiDB->GetEntry(fMotherA, fMotherZ));
	fParticleDaughter = new EduKitNucleus(NucleaiDB->GetEntry(fMotherA-4, fMotherZ-2));
 
	cout << "Masa nucleului mama este = " << fParticleMother->GetInfo()->Mass << endl;
	cout << "Masa nucleului fiica este: " << fParticleDaughter->GetInfo()->Mass << endl;
	cout << "Masa nucleului alfa este: " << malpha << endl;


	cout << "Excesul de masa al particulei initiale este: " << fParticleMother->GetInfo()->MassExcess << endl;
	cout << "Excesul de masa al particulei fiica: " << fParticleDaughter->GetInfo()->MassExcess << endl;

	S = fParticleDaughter->GetInfo()->MassExcess - fParticleMother->GetInfo()->MassExcess + Salpha;

	cout << "Energia de separare a particulei alpha este: " << S << endl;

	if(S > 0)
	{
		cout << "Nucleul ales de dumnevoastra nu poate sa se dezintegreze alpha!" << endl;
		return;
	}
	else
	{
		cout << "In continuare vom calcula energia cinetica a particulei alpha." << endl;

		m = fParticleMother->GetInfo()->MassExcess - fParticleDaughter->GetInfo()->MassExcess - EMalpha;
					
		Ealpha = m / (1 + malpha / (fParticleDaughter->GetInfo()->MassExcess + (fMotherA-4)*u) );			
		cout << "Energia cinetica a particulei alpha este: " << Ealpha << " [MeV]" << endl;

		Edaughter = (malpha / fParticleDaughter->GetInfo()->Mass) * Ealpha;
		cout << "Energia cinetica a particulei fiica este: " << Edaughter << " [MeV]" << endl;

		Palpha = sqrt(2 * Ealpha * malpha);
		cout << "Impulsul particulei alfa este: " << Palpha << " [MeV/c]" << endl;

		Pdaughter = Palpha;
		cout << "Imulsul particulei fiica este: " << Pdaughter << " [MeV/c]" << endl;
	}

	sigmaECinAlpha = 1.0/(sqrt(2*3.14159265) * Ealpha);
	cout << "sigma pentru energia cinetica a particulei alfa este: " << sigmaECinAlpha << endl;

	fMotherMarker = new TMarker;
	fMotherMarker->SetMarkerStyle(8);
    fMotherMarker->SetMarkerSize(16);
    fMotherMarker->SetMarkerColor(7);
    fMotherMarker->SetX(0);
    fMotherMarker->SetY(0);

    fMotherMarker->Draw();

    fMotherText = new TText(0, 0, fParticleMother->GetInfo()->Symbol.c_str());
    fMotherText->SetTextAlign(22);
    fMotherText->SetTextColor(1);
    fMotherText->SetTextFont(43);
    fMotherText->SetTextSize(50);
    fMotherText->SetTextAngle(0);
    fMotherText->Draw();


	fDaughterMarker = new TMarker;
    fDaughterMarker->SetMarkerStyle(8);
    fDaughterMarker->SetMarkerSize(12);
    fDaughterMarker->SetMarkerColor(2);
    fDaughterMarker->SetX(0);
    fDaughterMarker->SetY(0);

    fDaughterMarker->Draw();

    fDaughterArrow = new TArrow(0,0, 1, 1);
    fDaughterArrow->Draw();

    fDaughterText = new TText(0, 0, fParticleDaughter->GetInfo()->Symbol.c_str());
    fDaughterText->SetTextAlign(22);
    fDaughterText->SetTextColor(1);
    fDaughterText->SetTextFont(43);
    fDaughterText->SetTextSize(40);
    fDaughterText->SetTextAngle(0);
    fDaughterText->Draw();

    fAlphaMarker = new TMarker;
    fAlphaMarker->SetMarkerStyle(8);
    fAlphaMarker->SetMarkerSize(4);
    fAlphaMarker->SetMarkerColor(3);

    fAlphaMarker->SetX(0);
    fAlphaMarker->SetY(0);

    fAlphaMarker->Draw();

    fAlphaText = new TText(0, 0, "He");
    fAlphaText->SetTextAlign(22);
    fAlphaText->SetTextColor(1);
    fAlphaText->SetTextFont(43);
    fAlphaText->SetTextSize(20);
    fAlphaText->SetTextAngle(0);
    fAlphaText->Draw();

    fAlphaArrow = new TArrow(0,0, 1, 1);
    fAlphaArrow->Draw();

    // SCM Pad
    TCanvas* mainCanvas = gEduKit->GetCanvas();
	pad = (TCanvas*)mainCanvas->GetPad(3);
	pad->Range(0,0, 100, 50);
	gPad = pad;

    fSCMText = new TText(10, 45, "SCM Info Pad");
    fSCMText->SetTextColor(4);
    fSCMText->Draw();

    fSCMReactionText = new TLatex(10, 30, Form(" ^{%d}_{%d}%s #rightarrow ^{%d}_{%d}%s #plus ^{4}_{2}#alpha", 
    	fMotherA, fMotherZ, fParticleMother->GetInfo()->Symbol.c_str(),
    	fMotherA-4, fMotherZ-2, fParticleDaughter->GetInfo()->Symbol.c_str() ) );
    fSCMReactionText->Draw();

    fSCMSText = new TLatex(10, 25, Form("#sqrt{s} = %f (MeV)", fParticleMother->GetInfo()->Mass) );
    fSCMSText->Draw();
    fSCMEalphaText = new TLatex(10, 20, Form("E_{#alpha} = %f", EalphaR) );
    fSCMEalphaText->Draw();
	fSCMEdaughterText = new TLatex(10, 15, Form("E_{%s} = %f", fDaughterText->GetTitle(), EdaughterR) );
	fSCMEdaughterText->Draw();
	fSCMPalphaText = new TLatex(10, 10, Form("P_{#alpha} = %f", PalphaR) );
	fSCMPalphaText->Draw();
	fSCMAngleText = new TLatex(10, 2, Form("#Theta_{#alpha} = %f #circ", rotAngle) );
	fSCMAngleText->Draw();


	TH1F* histEAlpha = (TH1F*)gEduKit->NewPlot("TH1F", "histEAlpha", "Kinetic energy of alpha particle");
	histEAlpha->SetBins(100, 0, 10);
	histEAlpha->SetMinimum(1);
	histEAlpha->SetMaximum(fSimStepEnd);
	histEAlpha->SetStats(0);
	histEAlpha->SetLineColor(4);
	histEAlpha->GetXaxis()->SetTitle("Energy #alpha[MeV]");
	histEAlpha->GetYaxis()->SetTitle("N particles");
	histEAlpha->GetYaxis()->SetNdivisions(510);
	histEAlpha->Draw(); // we need to do this initially in order to add it to the current Plot Canvas

	gEduKit->ShowPlot("histEAlpha");

	sigmaECinDaughter = 1/(sqrt(2*3.14159265) * Edaughter);
	cout << "sigma pentru energia cinetica a particulei fiica este: " << sigmaECinDaughter << endl;

	TH1F* histEDaughter = (TH1F*)gEduKit->NewPlot("TH1F", "histEDaughter", "Kinetic energy of daughter particle");
	histEDaughter->SetBins(100, 0, 1.2);
	histEDaughter->SetMinimum(1);
	histEDaughter->SetMaximum(fSimStepEnd);
	histEDaughter->SetStats(0);
	histEDaughter->SetLineColor(4);
	histEDaughter->GetXaxis()->SetTitle("Energy #daughter[MeV]");
	histEDaughter->GetYaxis()->SetTitle("N particles");
	histEDaughter->GetYaxis()->SetNdivisions(510);
	histEDaughter->Draw(); // we need to do this initially in order to add it to the current Plot Canvas

	gEduKit->ShowPlot("histEDaughter");

	sigmaPAlpha = 1/(sqrt(2*3.14159265) * Palpha);
	cout << "sigma pentru impulsul particulei alfa este: " << sigmaPAlpha << endl;

	TH1F* histPAlpha = (TH1F*)gEduKit->NewPlot("TH1F", "histPAlpha", "Momentum of alpha particle");
	histPAlpha->SetBins(100, 150, 250);
	histPAlpha->SetMinimum(1);
	histPAlpha->SetMaximum(fSimStepEnd);
	histPAlpha->SetStats(0);
	histPAlpha->SetLineColor(4);
	histPAlpha->GetXaxis()->SetTitle("P #alpha[MeV/c]");
	histPAlpha->GetYaxis()->SetTitle("N particles");
	histPAlpha->GetYaxis()->SetNdivisions(510);
	histPAlpha->Draw(); // we need to do this initially in order to add it to the current Plot Canvas

	gEduKit->ShowPlot("histPAlpha");

	sigmaPDaughter = 1/(sqrt(2*3.14159265) * Pdaughter);
	cout << "sigma pentru impulsul particulei fiica este: " << sigmaPDaughter << endl;

	TH1F* histPDaughter = (TH1F*)gEduKit->NewPlot("TH1F", "histPDaughter", "Momentum of daughter particle");
	histPDaughter->SetBins(100, 150, 250);
	histPDaughter->SetMinimum(1);
	histPDaughter->SetMaximum(fSimStepEnd);
	histPDaughter->SetStats(0);
	histPDaughter->SetLineColor(4);
	histPDaughter->GetXaxis()->SetTitle("P #daughter[MeV/c]");
	histPDaughter->GetYaxis()->SetTitle("N particles");
	histPDaughter->GetYaxis()->SetNdivisions(510);
	histPDaughter->Draw(); // we need to do this initially in order to add it to the current Plot Canvas

	gEduKit->ShowPlot("histPDaughter");

	cout << fParticleDaughter->GetInfo()->Symbol << endl;

	gEduKit->UpdateDraw();
}

void alphadecay::NextStep(Int_t stepNumber)
{
	EalphaR = gRandom->Gaus(Ealpha, sigmaECinAlpha);
	TH1F* histEAlpha = (TH1F*)gEduKit->GetPlot("histEAlpha");
	histEAlpha->Fill(EalphaR);
	
	EdaughterR = TMath::Abs(m - EalphaR);
	TH1F* histEDaughter = (TH1F*)gEduKit->GetPlot("histEDaughter");
	histEDaughter->Fill(EdaughterR);
	
	PalphaR = TMath::Sqrt(EalphaR*EalphaR + 2.0*EalphaR*malpha);
	TH1F* histPAlpha = (TH1F*)gEduKit->GetPlot("histPAlpha");
	histPAlpha->Fill(PalphaR);
	
	PdaughterR = PalphaR;
	TH1F* histPDaughter = (TH1F*)gEduKit->GetPlot("histPDaughter");
	histPDaughter->Fill(PdaughterR);

	// --------- Drawing
	TCanvas* mainCanvas = gEduKit->GetCanvas();
	TPad* pad = (TCanvas*)mainCanvas->GetPad(1);
    double scale = 2 * TMath::Max(Pdaughter, Palpha);
	pad->Range(-scale,-scale, scale, scale);
	gPad = pad;

    fDaughterMarker->SetX(- PdaughterR);
    fDaughterMarker->SetY(0);
    fDaughterText->SetX(-PdaughterR);
    fDaughterText->SetY(0);

    fAlphaMarker->SetX( PalphaR);
    fAlphaMarker->SetY(0);
    fAlphaText->SetX(PalphaR);
    fAlphaText->SetY(0);

    rotAngle =  GradToRad(gRandom->Uniform(0, 360));
   
    cout << "rotAngle" << rotAngle << endl;

    RotateMarker(fDaughterMarker, rotAngle);
    RotateMarker(fAlphaMarker, rotAngle);

    RotateMarkerText(fDaughterText, rotAngle);
    RotateMarkerText(fAlphaText, rotAngle);

    fDaughterArrow->SetX2(fDaughterMarker->GetX());
    fDaughterArrow->SetY2(fDaughterMarker->GetY());

    fAlphaArrow->SetX2(fAlphaMarker->GetX());
    fAlphaArrow->SetY2(fAlphaMarker->GetY());

    drawInfoPad_SCM();

	gEduKit->UpdateDraw();
}

void alphadecay::drawInfoPad_SCM()
{
	if(!fSCMText) return;

	TCanvas* mainCanvas = gEduKit->GetCanvas();
	TPad* pad = (TCanvas*)mainCanvas->GetPad(3);
	pad->Range(0,0, 100, 50);
	gPad = pad;

	// no need to redraw the info
	//fSCMText->Draw();
	fSCMEalphaText->SetText(10, 20, Form("E_{#alpha} = %f (MeV)", EalphaR) );
	fSCMEdaughterText->SetText(10, 15, Form("E_{%s} = %f (MeV)", fDaughterText->GetTitle(), EdaughterR) );
	fSCMPalphaText->SetText(10, 10, Form("P_{#alpha} = %f (MeV)", PalphaR) );
	fSCMAngleText->SetText(10, 2, Form("#Theta_{#alpha} = %f #circ", TMath::RadToDeg()*rotAngle) );
}

void alphadecay::ClearAll()
{
	delete fParticleMother; fParticleMother=0;
	delete fParticleDaughter; fParticleDaughter=0;

	delete fMotherMarker; fMotherMarker=0;
    delete fMotherText; fMotherText=0;
    delete fDaughterMarker; fDaughterMarker=0;
    delete fDaughterText; fDaughterText=0;
    delete fAlphaMarker; fAlphaMarker=0;
    delete fAlphaText; fAlphaText=0;
    
    delete fSCMText; fSCMText = 0;
    delete fSCMReactionText; fSCMReactionText=0;
    delete fSCMSText;
    delete fSCMEalphaText;
	delete fSCMEdaughterText;
	delete fSCMPalphaText;
	delete fSCMAngleText;

    delete fDaughterArrow; fDaughterArrow=0;
    delete fAlphaArrow; fAlphaArrow=0;

	gEduKit->ClearPlots();

	gEduKit->UpdateDraw();
}

Int_t alphadecay::GetA() const
{
    return fMotherA;
}

Int_t alphadecay::GetZ() const
{
    return fMotherZ;
}

void alphadecay::SetDefaults()
{
    fSimStepStep = 1;
    fSimStepStart = 0;
    fSimStepEnd = 100;
    fSimTimeout = 200;

    fMotherA = 238;
	fMotherZ = 92;
}

void alphadecay::SetParams(Int_t A, Int_t Z)
{
	fMotherA = A;
	fMotherZ = Z;
}


EduKitExperimentParamsFrame* alphadecay::GetParamsFrame()
{
	return fParamsFrame;
}





/********************************************************************************
 *
 *        GUI  for the Experiments Settings
 *
 *
 *******************************************************************************/

//______________________________________________________________________________
AlphaDecayExperimentParamsFrame::AlphaDecayExperimentParamsFrame()
	: EduKitExperimentParamsFrame( gClient->GetRoot()),
	fExp(0),
	fEntryA(0),
	fEntryZ(0)
{
	SetCleanup(kDeepCleanup);

	SetLayoutManager(new TGMatrixLayout(this, 0, 2, 10));

	// Row Half-Life
	TGLabel* labelAtomA =  new TGLabel(this, " Atomic mass number (A):");
	fEntryA = new TGTextEntry(this, "238");
	AddFrame(labelAtomA);
	AddFrame(fEntryA);

	TGLabel* labelAtomNumber =  new TGLabel(this, "Atomic number (Z):");
	fEntryZ = new TGTextEntry(this, "92");
	AddFrame(labelAtomNumber);
	AddFrame(fEntryZ);

	onDefaulted();

	//fEntrySimEventsNumber->Connect("TextChanged(const char*)",  "AlphaDecayExperimentParamsFrame", this, "UpdateTimeStepLabel()");

	Resize();
}

AlphaDecayExperimentParamsFrame::AlphaDecayExperimentParamsFrame(alphadecay* exp)
	: EduKitExperimentParamsFrame( gClient->GetRoot()),
	fExp(exp),
	fEntryA(0),
	fEntryZ(0)
{
	SetCleanup(kDeepCleanup);

	SetLayoutManager(new TGMatrixLayout(this, 0, 2, 10));

	// Row Half-Life
	TGLabel* labelAtomA =  new TGLabel(this, "Atomic mass number (A):");
	fEntryA = new TGTextEntry(this, "238");
	AddFrame(labelAtomA);
	AddFrame(fEntryA);

	TGLabel* labelAtomNumber =  new TGLabel(this, "Atomic number (Z):");
	fEntryZ = new TGTextEntry(this, "92");
	AddFrame(labelAtomNumber);
	AddFrame(fEntryZ);

	onDefaulted();

	Resize(400, 200);
}

void AlphaDecayExperimentParamsFrame::onAccepted()
{
	TString nucleaiAStr(fEntryA->GetText());
	TString nucleaiZStr(fEntryZ->GetText());

	if(fExp){
		// set basic simulation data
		fExp->SetParams(nucleaiAStr.Atoi(), nucleaiZStr.Atoi());
	}
}

void AlphaDecayExperimentParamsFrame::onCanceled()
{
	// do nothing
}

void AlphaDecayExperimentParamsFrame::onDefaulted()
{
	if(!fExp) return;

	fExp->SetDefaults();

	LoadData();
}

void AlphaDecayExperimentParamsFrame::LoadData()
{
	if(!fExp) return;

	fEntryA->SetText( Form("%d", fExp->GetA()) );
	fEntryZ->SetText( Form("%d", fExp->GetZ()) );

	Layout();
}