/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitExperiment.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#ifndef _EDUKIT_NUCLEUS_SPRITE_H_
#define _EDUKIT_NUCLEUS_SPRITE_H_

#include <TCanvas.h>

class EduKitNucleus;

class EduKitNucleusSprite
{
public:
	

protected:
	TCanvas *fCanvas;
	EduKitNucleus *fNucleus;
	char fDrawStyle;

	ClassDef(EduKitNucleusSprite,0)
};

#endif
