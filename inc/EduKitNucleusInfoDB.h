/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitExperiment.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#ifndef _EDUKIT_NUCLEUS_INFO_DB_H_
#define _EDUKIT_NUCLEUS_INFO_DB_H_

#include <vector>
#include <string>

class EduKitNucleusInfo;

class EduKitNucleusInfoDB
{
public:
    EduKitNucleusInfoDB(const char* filename);
    ~EduKitNucleusInfoDB();

    EduKitNucleusInfo* GetEntry(int a, int z) const;

    std::string GetName(int a, int z) const;
 
    std::string GetSymbol(int a, int z) const;
       
    double GetMassInAMU(int a, int z) const;
   
    double GetMassInMeV(int a, int z) const;
    
    double GetMassExcessInMeV(int a, int z) const;
       
    double GetMassDefectInAMU(int a, int z) const;
   
    int GetId(int a, int z) const;
   
    double GetCharge(int a, int z) const;
    

protected:
	std::vector<EduKitNucleusInfo*> fNucleusInfoList;

public:
	ClassDef(EduKitNucleusInfoDB, 0)
};

#endif