/*****************************************************************************************
 *                                                                                       
 *  This file (DecaysExperimentParamsFrame.cxx) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

#include <TMath.h>
#include <TPad.h>
#include <TMarker.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TRandom.h>
#include <TText.h>
#include <TArrow.h>
#include <TString.h>
#include <TLatex.h>

#include "EduKitMain.h"
#include "EduKitNucleus.h"
#include "EduKitNucleusInfo.h"
#include "EduKitNucleusInfoDB.h"
#include "FissionClasses.h"
#include "EduKitLesson.h"

#define PI 3.1415

using namespace std;
 
class FissionProbability
{

public:

FissionProbability()
{
	float i; 
	float READ[300];
	int contor=0;

	const char*fisier = Form("%s/U5YALI.DAT", gEduKit->GetCurrentLesson()->GetResourceDir()); //atribuie nume intern fisierului extern
	ifstream infile(fisier); // infile(fisier) are valoarea numarului de caractere din fisier

	// if (infile.is_open()) cout << "Fisier deschis!!!" << endl;
	// else cout << "Fisier negasit!" << endl;

 	while (infile >> i) // cat timp i este in fisier, citeste caractere de tip i la fiecare iteratie
 	{ 
 	
	READ[contor] = i; // stocheaza in vectorul READ[iteratie curenta] valoarea numarului i citita la iteratia curenta
	contor = contor + 1; // incrementeaza pozitia vectorului de stocat
 	}
 	// am terminat de citit fisierul si toate cifrele sunt stocate in vectorul READ
 
	for (int j=0;j<95;j++) //alocam cele 3 * 94 date din vectorul READ pe vectori separati
	{
	fA[j] = READ[j*3]; // in vectorul fA va fi masa atomica pentru nucleul 1 prompt de la pozitia j
	fP[j] = READ[j*3 + 1]; // in vectorul fP va fi probabilitatea de fisiune in varianta fA[j] 
	}

	cout << "Valoarea de control pentru ultimul fP tb sa fie .00216: " << fP[94] << endl;
}

 ~FissionProbability()
{}

int GetA(int poz)
{
	return fA[poz];
}

float GetP(int poz)
{
	return fP[poz];
}

int GetNewSimulatedA1()
{
	double randNo = gRandom->Uniform(200);
	float sumaDistributie = 0.0;

for (int j=0; j<95; j++)
{
	sumaDistributie = sumaDistributie + GetP(j);
	if (sumaDistributie >= randNo) // verificam ce varianta de fisiune a avut loc (respectand distributia)
	{
		return fA[j];
	}
}

	return 0;
}

private:
	int fA[95];
	float fP[95];
};


float grad_to_rad(float grade)
{
	float rad = grade*PI/180.0;

	return rad;
}

ClassImp(FissionExperiment)

//______________________________________________________________________________
FissionExperiment::FissionExperiment()
	: EduKitExperiment("fission", "Simulation of the fission event for uranium 235"),
	fU235FissionPROB(0),
	fNucleusDB(0),
	fFragment1(0),
	fFragment2(0),
	fSimulatedA1(0),
	fSimulatedZ1(0),
	fMarkerU(0),
	fMarkerF1(0),
	fMarkerF2(0),
	fTextU(0),
	fTextF1(0),
	fTextF2(0),
	fMarkerF1N1(0),
	fMarkerF1N2(0),
	fMarkerF1N3(0),
	fMarkerF2N1(0),
	fMarkerF2N2(0),
	fMarkerF2N3(0),
	fArF1(0),
	fArF2(0),
	fArF1N1(0),
	fArF1N2(0),
	fArF1N3(0),
	fArF2N1(0),
	fArF2N2(0),
	fArF2N3(0),
	fReactionInfo(0),
	fFragment1Info(0),
	fFragment2Info(0)
{ 	

	printf("Done.\n Diving Canvas...\n");
 	TPad* pad = gEduKit->GetCanvas();
    pad->Divide(1,2);

	SetDefaults();
}

FissionExperiment::~FissionExperiment() 
{
	ClearAll(); 
}

void FissionExperiment::SetDefaults()
{
		
	fSimStepStart = 0;
	fSimStepStep  = 1;
	fSimStepEnd   = 1000;
	fSimTimeout   = 330;
}

void FissionExperiment::Init()
{
	// make active the current canvas
	TPad* pad = gEduKit->GetCanvas();
    pad->cd(1);
    pad = (TCanvas*)pad->GetPad(1);
    gPad = pad;
	
	if(!pad) return;
	pad->Range(0,0, 400,600); 

	fMarkerU = new TMarker;
	fMarkerU->SetMarkerStyle(8);
	fMarkerU->SetMarkerSize(16);
	fMarkerU->SetMarkerColor(12);

	fMarkerU->SetX(200);
	fMarkerU->SetY(300);

	fMarkerU->Draw();

	fTextU = new TText(200, 300, "U");
    fTextU->SetTextAlign(22);
    fTextU->SetTextColor(10);
    fTextU->SetTextFont(43);
    fTextU->SetTextSize(50);
    fTextU->SetTextAngle(0);
    fTextU->Draw();
    

fU235FissionPROB = new FissionProbability;
fNucleusDB = gEduKit->NucleaiDB();

// bottom Pad
    pad = gEduKit->GetCanvas();
	pad = (TCanvas*)pad->GetPad(2);
	pad->Range(0,0, 100, 30);
	gPad = pad;

    fReactionInfo = new TLatex();
	fFragment1Info = new TLatex();
	fFragment2Info = new TLatex();

	fReactionInfo->Draw();
	fFragment1Info->Draw();
	fFragment2Info->Draw();


TH1I* histFission = (TH1I*)gEduKit->NewPlot("TH1I", "histFission", "Histogram of atomic mass fragments emerged in the fission process");
	
	histFission->SetBins(105,65,170);
	histFission->SetMinimum(1);
	histFission->SetMaximum(80);
	histFission->SetStats(0);
	histFission->SetLineColor(4);
	histFission->GetXaxis()->SetTitle("Atomic mass of fragments [A]");
	histFission->GetYaxis()->SetTitle("Frequency");
	histFission->Draw(); // we need to do this initially in order to add it to the current Plot Canvas

	gEduKit->ShowPlot("histFission");

TH1F* QrFission = (TH1F*)gEduKit->NewPlot("TH1F", "QrFission", "Histogram of Energy emerged in the fission process");
	
	QrFission->SetBins(70,140,210);
	QrFission->SetMinimum(1);
	QrFission->SetMaximum(160);
	QrFission->SetStats(0);
	QrFission->SetLineColor(4);
	QrFission->GetXaxis()->SetTitle("Energy released [MeV]");
	QrFission->GetYaxis()->SetTitle("Frequency");
	QrFission->Draw(); // we need to do this initially in order to add it to the current Plot Canvas

	gEduKit->ShowPlot("QrFission");


TH1I* neutronNo = (TH1I*)gEduKit->NewPlot("TH1I", "neutronNo", "Histogram of the nomber of neutrons emerged in the fission process");
	
	neutronNo->SetBins(6,1,6);
	neutronNo->SetMinimum(1);
	neutronNo->SetMaximum(350);
	neutronNo->SetStats(0);
	neutronNo->SetLineColor(4);
	neutronNo->GetXaxis()->SetTitle("Nomber of neutrons");
	neutronNo->GetYaxis()->SetTitle("Frequency");
	neutronNo->Draw(); // we need to do this initially in order to add it to the current Plot Canvas

	gEduKit->ShowPlot("neutronNo");

}

void FissionExperiment::ClearAll()
{
	delete fU235FissionPROB; fU235FissionPROB=0;
	delete fNucleusDB; fNucleusDB=0;
	delete fMarkerU; fMarkerU=0;
	delete fTextU; fTextU=0;

	delete fReactionInfo;
	delete fFragment1;
	delete fFragment2;

	 gEduKit->ClearPlots();

	 gEduKit->UpdateDraw();
}

void FissionExperiment::NextStep(Int_t stepNumber)
{
	TCanvas* mainCanvas = gEduKit->GetCanvas();
	mainCanvas->cd(1);
    TPad* pad = (TCanvas*)mainCanvas->GetPad(1);
    gPad = pad;

	TH1I* histFission = (TH1I*)gEduKit->GetPlot("histFission");
	TH1F* QrFission = (TH1F*)gEduKit->GetPlot("QrFission");
	TH1I* neutronNo = (TH1I*)gEduKit->GetPlot("neutronNo");

fSimulatedA1 = fU235FissionPROB->GetNewSimulatedA1();
fSimulatedZ1 = floor(fSimulatedA1*92.0/235.0-0.5) + 0.5;

fFragment1 = new EduKitNucleus(fNucleusDB->GetEntry(fSimulatedA1, fSimulatedZ1));
fFragment2 = new EduKitNucleus(fNucleusDB->GetEntry(235-fSimulatedA1, 92-fSimulatedZ1));
	
	float EM_U235=fNucleusDB->GetMassExcessInMeV(235,92);
	float EM_F1=fNucleusDB->GetMassExcessInMeV(fSimulatedA1, fSimulatedZ1);
	float EM_F2=fNucleusDB->GetMassExcessInMeV(235-fSimulatedA1, 92-fSimulatedZ1);
	float MassF1= (EM_F1/931.494 + fSimulatedA1)*931.494;
	float MassF2= (EM_F2/931.494 + 235-fSimulatedA1)*931.494;

float Qr = EM_U235 - EM_F1 - EM_F2;
float fragmentMomentum=sqrt((2.0*MassF1*MassF2*Qr)/(MassF1+MassF2));

		float phi1 = rand()%360;
		float phi2 = 360-phi1;

	fFragment1->SetMomentum(fragmentMomentum);
	fFragment2->SetMomentum(fragmentMomentum);

	fFragment1->SetMomentumPhi(phi1);
	fFragment2->SetMomentumPhi(phi2);

float fragment1TEnergy = (fragmentMomentum*fragmentMomentum)/(2*MassF1);
float fragment2TEnergy = (fragmentMomentum*fragmentMomentum)/(2*MassF2);

fFragment1->SetKEnergy(fragment1TEnergy);
fFragment2->SetKEnergy(fragment2TEnergy);

int nEmisiF1;
int nEmisiF2;

		if (fSimulatedA1>=235-fSimulatedA1)
		{
		nEmisiF1 = 0.5 + gRandom->Uniform(3);
		nEmisiF2 = 0.5 + gRandom->Uniform(2);
		}
		else 
		{
		nEmisiF1 = 0.5 + gRandom->Uniform(2);
		nEmisiF2 = 0.5 + gRandom->Uniform(3);
		}
	

	histFission->Fill(fSimulatedA1);
	histFission->Fill(235-fSimulatedA1);

	QrFission->Fill(Qr);

	neutronNo->Fill(nEmisiF1+nEmisiF2);

	// Animation

	if (fSimulatedA1>235-fSimulatedA1)
	{
		fMarkerSizeF1 = 10;
		fMarkerSizeF2 = 6;
	}
	else if (fSimulatedA1<235-fSimulatedA1)
	{
		fMarkerSizeF1 = 6;
		fMarkerSizeF2 = 10;
	}
	else
	{
		fMarkerSizeF1 = 8;
		fMarkerSizeF2 = 8;	
	}

	phi1 = grad_to_rad(phi1);

	fXF1 = 200+ 150* cos(phi1);
	fYF1 = 300+ 150* sin(phi1);

	fXF2 = 200- 150* cos(phi1);
	fYF2 = 300- 150* sin(phi1);

	fXArF1 = 200+ 50* cos(phi1);
	fYArF1 = 300+ 50* sin(phi1);

	fXArF2 = 200- 50* cos(phi1);
	fYArF2 = 300- 50* sin(phi1);

	if((fTextF1==0)&&(fTextF2==0))
	{
	
	fMarkerF1 = new TMarker;
	fMarkerF1->SetMarkerStyle(8);
	fMarkerF1->SetMarkerSize(fMarkerSizeF1);
	fMarkerF1->SetMarkerColor(39);

	fMarkerF1->SetX(fXF1);
	fMarkerF1->SetY(fYF1);

	fMarkerF1->Draw();

	fArF1 = new TArrow(fXArF1, fYArF1, (200+fXF1)/2, (300+fYF1)/2, 0.02,"|>");
    //ar2->SetAngle(40);
    fArF1->SetLineWidth(2);
    fArF1->Draw();

	fMarkerF2 = new TMarker;
	fMarkerF2->SetMarkerStyle(8);
	fMarkerF2->SetMarkerSize(fMarkerSizeF2);
	fMarkerF2->SetMarkerColor(39);

	fMarkerF2->SetX(fXF2);
	fMarkerF2->SetY(fYF2);

	fMarkerF2->Draw();

	fArF2 = new TArrow(fXArF2, fYArF2, (200+fXF2)/2, (300+fYF2)/2, 0.02,"|>");
    fArF2->SetLineWidth(2);
    fArF2->Draw();

	fMarkerF1N1 = new TMarker;
	fMarkerF1N1->SetMarkerStyle(8);
	fMarkerF1N1->SetMarkerSize(1);
	fMarkerF1N1->SetMarkerColor(14);

	fMarkerF1N1->SetX(fXF1+30);
	fMarkerF1N1->SetY(fYF1+30);

	fMarkerF1N2 = new TMarker;
	fMarkerF1N2->SetMarkerStyle(8);
	fMarkerF1N2->SetMarkerSize(1);
	fMarkerF1N2->SetMarkerColor(14);

	fMarkerF1N2->SetX(fXF1-30);
	fMarkerF1N2->SetY(fYF1-30);

	fMarkerF1N3 = new TMarker;
	fMarkerF1N3->SetMarkerStyle(8);
	fMarkerF1N3->SetMarkerSize(1);
	fMarkerF1N3->SetMarkerColor(14);

	fMarkerF1N3->SetX(fXF1-30);
	fMarkerF1N3->SetY(fYF1+30);

	if (nEmisiF1>2)
	{
		fMarkerF1N1->Draw();
		fMarkerF1N2->Draw();
		fMarkerF1N3->Draw();
	}
	else if (nEmisiF1==2)
	{
		fMarkerF1N1->Draw();
		fMarkerF1N2->Draw();
	}
	else if (nEmisiF1==1)
	{
		fMarkerF1N1->Draw();
	}


	fMarkerF2N1 = new TMarker;
	fMarkerF2N1->SetMarkerStyle(8);
	fMarkerF2N1->SetMarkerSize(1);
	fMarkerF2N1->SetMarkerColor(14);

	fMarkerF2N1->SetX(fXF2+30);
	fMarkerF2N1->SetY(fYF2+30);

	fMarkerF2N2 = new TMarker;
	fMarkerF2N2->SetMarkerStyle(8);
	fMarkerF2N2->SetMarkerSize(1);
	fMarkerF2N2->SetMarkerColor(14);

	fMarkerF2N2->SetX(fXF2-30);
	fMarkerF2N2->SetY(fYF2-30);

	fMarkerF2N3 = new TMarker;
	fMarkerF2N3->SetMarkerStyle(8);
	fMarkerF2N3->SetMarkerSize(1);
	fMarkerF2N3->SetMarkerColor(14);

	fMarkerF2N3->SetX(fXF2-30);
	fMarkerF2N3->SetY(fYF2+30);

	if (nEmisiF2>2)
	{
		fMarkerF2N1->Draw();
		fMarkerF2N2->Draw();
		fMarkerF2N3->Draw();
	}
	else if (nEmisiF2==2)
	{
		fMarkerF2N1->Draw();
		fMarkerF2N2->Draw();
	}
	else if (nEmisiF2==1)
	{
		fMarkerF2N1->Draw();
	}


	fTextF1 = new TText(fXF1, fYF1, fNucleusDB->GetSymbol(fSimulatedA1, fSimulatedZ1).c_str());
    fTextF1->SetTextAlign(22);
    fTextF1->SetTextColor(10);
    fTextF1->SetTextFont(43);
    fTextF1->SetTextSize(35);
    fTextF1->SetTextAngle(0);
    fTextF1->Draw();

    fTextF2 = new TText(fXF2, fYF2, fNucleusDB->GetSymbol(235-fSimulatedA1, 92-fSimulatedZ1).c_str());
    fTextF2->SetTextAlign(22);
    fTextF2->SetTextColor(10);
    fTextF2->SetTextFont(43);
    fTextF2->SetTextSize(35);
    fTextF2->SetTextAngle(0);
    fTextF2->Draw();

	} 

	else
	{
	
	delete fMarkerF1; fMarkerF1=0;
    fMarkerF1 = new TMarker;
	fMarkerF1->SetMarkerStyle(8);
	fMarkerF1->SetMarkerSize(fMarkerSizeF1);
	fMarkerF1->SetMarkerColor(39);

	fMarkerF1->SetX(fXF1);
	fMarkerF1->SetY(fYF1);

	fMarkerF1->Draw();

	delete fArF1; fArF1=0;
	fArF1 = new TArrow(fXArF1, fYArF1, (200+fXF1)/2, (300+fYF1)/2, 0.02,"|>");
    fArF1->SetLineWidth(2);
    fArF1->Draw();

	delete fMarkerF2; fMarkerF2=0;
	fMarkerF2 = new TMarker;
	fMarkerF2->SetMarkerStyle(8);
	fMarkerF2->SetMarkerSize(fMarkerSizeF2);
	fMarkerF2->SetMarkerColor(39);

	fMarkerF2->SetX(fXF2);
	fMarkerF2->SetY(fYF2);

	fMarkerF2->Draw();

	delete fArF2; fArF2=0;
	fArF2 = new TArrow(fXArF2, fYArF2, (200+fXF2)/2, (300+fYF2)/2, 0.02,"|>");
    fArF2->SetLineWidth(2);
    fArF2->Draw();

	delete fMarkerF1N1; fMarkerF1N1=0;
	fMarkerF1N1 = new TMarker;
	fMarkerF1N1->SetMarkerStyle(8);
	fMarkerF1N1->SetMarkerSize(1);
	fMarkerF1N1->SetMarkerColor(14);

	fMarkerF1N1->SetX(fXF1+30);
	fMarkerF1N1->SetY(fYF1+30);

	delete fMarkerF1N2; fMarkerF1N2=0;
	fMarkerF1N2 = new TMarker;
	fMarkerF1N2->SetMarkerStyle(8);
	fMarkerF1N2->SetMarkerSize(1);
	fMarkerF1N2->SetMarkerColor(14);

	fMarkerF1N2->SetX(fXF1-30);
	fMarkerF1N2->SetY(fYF1-30);

	delete fMarkerF1N3; fMarkerF1N3=0;
	fMarkerF1N3 = new TMarker;
	fMarkerF1N3->SetMarkerStyle(8);
	fMarkerF1N3->SetMarkerSize(1);
	fMarkerF1N3->SetMarkerColor(14);

	fMarkerF1N3->SetX(fXF1-30);
	fMarkerF1N3->SetY(fYF1+30);

	if (nEmisiF1>2)
	{
		fMarkerF1N1->Draw();
		fMarkerF1N2->Draw();
		fMarkerF1N3->Draw();
	}
	else if (nEmisiF1==2)
	{
		fMarkerF1N1->Draw();
		fMarkerF1N2->Draw();
	}
	else if (nEmisiF1==1)
	{
		fMarkerF1N1->Draw();
	}

	delete fMarkerF2N1; fMarkerF2N1=0;
	fMarkerF2N1 = new TMarker;
	fMarkerF2N1->SetMarkerStyle(8);
	fMarkerF2N1->SetMarkerSize(1);
	fMarkerF2N1->SetMarkerColor(14);

	fMarkerF2N1->SetX(fXF2+30);
	fMarkerF2N1->SetY(fYF2+30);

	delete fMarkerF2N2; fMarkerF2N2=0;
	fMarkerF2N2 = new TMarker;
	fMarkerF2N2->SetMarkerStyle(8);
	fMarkerF2N2->SetMarkerSize(1);
	fMarkerF2N2->SetMarkerColor(14);

	fMarkerF2N2->SetX(fXF2-30);
	fMarkerF2N2->SetY(fYF2-30);

	delete fMarkerF2N3; fMarkerF2N3=0;
	fMarkerF2N3 = new TMarker;
	fMarkerF2N3->SetMarkerStyle(8);
	fMarkerF2N3->SetMarkerSize(1);
	fMarkerF2N3->SetMarkerColor(14);

	fMarkerF2N3->SetX(fXF2-30);
	fMarkerF2N3->SetY(fYF2+30);

	if (nEmisiF2>2)
	{
		fMarkerF2N1->Draw();
		fMarkerF2N2->Draw();
		fMarkerF2N3->Draw();
	}
	else if (nEmisiF2==2)
	{
		fMarkerF2N1->Draw();
		fMarkerF2N2->Draw();
	}
	else if (nEmisiF2==1)
	{
		fMarkerF2N1->Draw();
	}

	delete fTextF1; fTextF1=0;
	fTextF1 = new TText(fXF1, fYF1, fNucleusDB->GetSymbol(fSimulatedA1, fSimulatedZ1).c_str());
    fTextF1->SetTextAlign(22);
    fTextF1->SetTextColor(10);
    fTextF1->SetTextFont(43);
    fTextF1->SetTextSize(35);
    fTextF1->SetTextAngle(0);
    fTextF1->Draw();

    delete fTextF2; fTextF2=0;
    fTextF2 = new TText(fXF2, fYF2, fNucleusDB->GetSymbol(235-fSimulatedA1, 92-fSimulatedZ1).c_str());
    fTextF2->SetTextAlign(22);
    fTextF2->SetTextColor(10);
    fTextF2->SetTextFont(43);
    fTextF2->SetTextSize(35);
    fTextF2->SetTextAngle(0);
    fTextF2->Draw();

	} 

// bottom Pad
    mainCanvas = gEduKit->GetCanvas();
	pad = (TCanvas*)mainCanvas->GetPad(2);
	pad->Range(0,0, 100, 10);
	gPad = pad;

	fReactionInfo->SetText(10, 6, Form(" ^{%d}_{%d}%s #rightarrow ^{%d}_{%d}%s #plus ^{%d}_{%d}%s #plus %d #times ^{0}_{0}n #plus %f (MeV)", 
    	235, 92, "U",
    	fFragment1->GetInfo()->A,  fFragment1->GetInfo()->Z, fFragment1->GetInfo()->Symbol.c_str(),
    	fFragment2->GetInfo()->A,  fFragment2->GetInfo()->Z, fFragment2->GetInfo()->Symbol.c_str(),  
    	nEmisiF1+nEmisiF2,
    	Qr) );

	fFragment1Info->SetText(10, 4, Form("%s: T_{Kin} = %f (MeV) \tP = %f(MeV) \t#Theta = %f \t%d neutrons", 
		fFragment1->GetInfo()->Symbol.c_str(), fFragment1->GetKEnergy(), fFragment1->GetMomentum(), fFragment1->GetMomentumPhi(), nEmisiF1) );
	
	fFragment2Info->SetText(10, 2, Form("%s: T_{Kin} = %f (MeV) \tP = %f(MeV) \t#Theta = %f \t%d neutrons", 
		fFragment2->GetInfo()->Symbol.c_str(), fFragment2->GetKEnergy(), fFragment2->GetMomentum(), fFragment2->GetMomentumPhi(), nEmisiF2) );

delete fFragment1; fFragment1=0; 
delete fFragment2; fFragment2=0;

	// fMarkerU->SetX(200-stepNumber);
	// fMarkerU->SetY(300-stepNumber);

	gEduKit->UpdateDraw();

}

/********************************************************************************
 *                                                                              *
 *        GUI  for the Experiments Settings                                     *
 *                                                                              *
 *                                                                              *
 *******************************************************************************/

//______________________________________________________________________________

/*
DecaysExperimentParamsFrame::DecaysExperimentParamsFrame()
	: EduKitExperimentParamsFrame( gClient->GetRoot()),
	fExp(0),
	fEntryAtomHalfLife(0),
	fEntryAtomsTotalNumber(0),
	fEntrySimEventsNumber(0)
{
	SetCleanup(kDeepCleanup);

	SetLayoutManager(new TGMatrixLayout(this, 0, 2, 10));

	// Row Half-Life
	TGLabel* labelAtomHL =  new TGLabel(this, "Half-Life:");
	fEntryAtomHalfLife = new TGTextEntry(this, "30");
	AddFrame(labelAtomHL);
	AddFrame(fEntryAtomHalfLife);

	// Row Nucleai Total Number
	TGLabel* labelAtomsNumber =  new TGLabel(this, "Nucleai:");
	fEntryAtomsTotalNumber = new TGTextEntry(this, "400");
	AddFrame(labelAtomsNumber);
	AddFrame(fEntryAtomsTotalNumber);

	// Row Sim events number
	TGLabel* labelSimEventsNumber = new TGLabel(this, "Events:");
	fEntrySimEventsNumber = new TGTextEntry(this, "10");
	AddFrame(labelSimEventsNumber);
	AddFrame(fEntrySimEventsNumber);

	onDefaulted();

	fEntrySimEventsNumber->Connect("TextChanged(const char*)",  "DecaysExperimentParamsFrame", this, "UpdateTimeStepLabel()");

	Resize();
}

DecaysExperimentParamsFrame::DecaysExperimentParamsFrame(DecaysExperiment* exp)
	: EduKitExperimentParamsFrame( gClient->GetRoot()),
	fExp(exp),
	fEntryAtomHalfLife(0),
	fEntryAtomsTotalNumber(0),
	fEntrySimEventsNumber(0)
{
	SetCleanup(kDeepCleanup);

	SetLayoutManager(new TGMatrixLayout(this, 0, 2, 10));

	// Row Half-Life
	TGLabel* labelAtomHL =  new TGLabel(this, "Half-Life:");
	fEntryAtomHalfLife = new TGTextEntry(this, "30");
	AddFrame(labelAtomHL);
	AddFrame(fEntryAtomHalfLife);

	fEntryAtomHalfLife->Resize(100);

	// Row Nucleai Total Number
	TGLabel* labelAtomsNumber =  new TGLabel(this, "Nucleai:");
	fEntryAtomsTotalNumber = new TGTextEntry(this, "400");
	AddFrame(labelAtomsNumber);
	AddFrame(fEntryAtomsTotalNumber);
	fEntryAtomsTotalNumber->Resize(100);

	// Row Sim events number
	TGLabel* labelSimEventsNumber = new TGLabel(this, "Number of Events:");
	fEntrySimEventsNumber = new TGTextEntry(this, "10");
	AddFrame(labelSimEventsNumber);
	AddFrame(fEntrySimEventsNumber);
	fEntrySimEventsNumber->Resize(100);

	onDefaulted();

	fEntrySimEventsNumber->Connect("TextChanged(const char*)",  "DecaysExperimentParamsFrame", this, "UpdateTimeStepLabel()");
	

	Resize(200);
}

void DecaysExperimentParamsFrame::onAccepted()
{
	TString nucleaiStr(fEntryAtomsTotalNumber->GetText());
	TString hlStr(fEntryAtomHalfLife->GetText());

	if(fExp){
		// set basic simulation data
		fExp->SetAtomsNumber(nucleaiStr.Atoi());
		fExp->SetAtomsHalfLife(hlStr.Atof());
	
		fExp->SetTimeEnd( TString(fEntrySimEventsNumber->GetText()).Atoi() );
	}
}

void DecaysExperimentParamsFrame::onCanceled()
{
	// do nothing
}

void DecaysExperimentParamsFrame::onDefaulted()
{
	if(!fExp) return;

	fExp->SetDefaults();

	LoadData();
}

void DecaysExperimentParamsFrame::LoadData()
{
	if(!fExp) return;

	fEntryAtomHalfLife->SetText( Form("%f", fExp->GetAtomsHalfLife()) );
	fEntryAtomsTotalNumber->SetText( Form("%d", fExp->GetAtomsNumber()) );

	fEntrySimEventsNumber->SetText( Form("%d", fExp->GiteStepEnd()) );
}

Float_t DecaysExperimentParamsFrame::GetTimeStep() const
{
	Int_t nevents = TString(fEntrySimEventsNumber->GetText()).Atoi();

	if(nevents<1) return 0.0;

	Float_t maxTime, minTime;
	minTime = fExp->GetTimeStart();
	maxTime = fExp->GiteStepEnd();

	if(minTime==maxTime) return 0.0;

	Float_t timeStep = (maxTime-minTime)/nevents;


	return timeStep;
}
*/