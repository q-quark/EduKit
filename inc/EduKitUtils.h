/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitUtils.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
 

 // thieft from TEveUtil.h

#ifndef _EDUKIT_UTILS_H__
#define _EDUKIT_UTILS_H__ 

class EduKitUtils
{
public:
	virtual ~EduKitUtils(){ }
	
static Bool_t CheckMacro(const char* mac);
static void   AssertMacro(const char* mac);
static void   Macro(const char* mac);
static void   LoadMacro(const char* mac);
};
 
#endif