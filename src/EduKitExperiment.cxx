/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitExperiment.cxx) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#include "EduKitExperiment.h"

ClassImp(EduKitExperiment)
EduKitExperiment::EduKitExperiment(const char* name ,const char* title)
	: TNamed(name, title),
	fSimStepStart(0),
	fSimStepStep(1),
	fSimStepEnd(100),
	fSimTimeout(33)
{

}

EduKitExperiment::~EduKitExperiment() { }

void EduKitExperiment::Init() { AbstractMethod("Init"); }
void EduKitExperiment::NextStep(Int_t stepNumber){ AbstractMethod("NextStep"); }
void EduKitExperiment::ClearAll(){ AbstractMethod("ClearAll"); }

// return the name of the class name that implements the GUI that sets up the experiment's input parameters
// it MUST derive directly from TGCompositeFrame
EduKitExperimentParamsFrame* EduKitExperiment::GetParamsFrame(){ AbstractMethod("GetParamsFrame"); return 0; }

// Prefered Settings for this experiment
Int_t EduKitExperiment::GetStepStart() const{ return fSimStepStart; }
Int_t EduKitExperiment::GetStepStep() const{ return fSimStepStep; }
Int_t EduKitExperiment::GetStepEnd() const{ return fSimStepEnd; }
Long_t EduKitExperiment::GetTimeout() const { return fSimTimeout; }

void EduKitExperiment::SetStepStart(Int_t tmin){ fSimStepStart = tmin; }
void EduKitExperiment::SetStepStep(Int_t tstep){ fSimStepStep  = tstep; }
void EduKitExperiment::SetStepEnd(Int_t tmax){ fSimStepEnd = tmax; }
void EduKitExperiment::SetTimeout(Long_t timeout) { fSimTimeout = timeout; }

void EduKitExperiment::SetDefaults(){ AbstractMethod("SetDefaults"); }