/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitExperiment.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#include "EduKitNucleus.h"

ClassImp(EduKitNucleus);

EduKitNucleus::EduKitNucleus(): fX(0.0), fY(0.0), fKenergy(0.0), fMomentum(0.0), fNucleusInfo(0), fPhi(0)
 {}

EduKitNucleus::EduKitNucleus(EduKitNucleusInfo* NucleusInfo, float x, float y, Double_t ke, Double_t p, float phi)
    : fX(x), fY(y), fNucleusInfo(NucleusInfo), fKenergy(ke), fMomentum(p), fPhi(phi) 
 {}

EduKitNucleus::~EduKitNucleus() 
 {}
 
void EduKitNucleus::SetX(float x) {fX = x;}
void EduKitNucleus::SetY(float y) {fY = y;}
void EduKitNucleus::SetInfo(EduKitNucleusInfo* NucleusInfo) {fNucleusInfo = NucleusInfo;}
void EduKitNucleus::SetMomentum(Double_t p) {fMomentum = p;}
void EduKitNucleus::SetKEnergy(Double_t ke) {fKenergy = ke;}
void EduKitNucleus::SetMomentumPhi(float phi) {fPhi = phi;}

float EduKitNucleus::GetX() {return fX;}
float EduKitNucleus::GetY() {return fY;}
EduKitNucleusInfo* EduKitNucleus::GetInfo() {return fNucleusInfo;}
Double_t EduKitNucleus::GetMomentum() {return fMomentum;}
Double_t EduKitNucleus::GetKEnergy() {return fKenergy;}
float EduKitNucleus::GetMomentumPhi() {return fPhi;}