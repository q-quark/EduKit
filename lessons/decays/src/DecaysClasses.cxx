/*****************************************************************************************
 *                                                                                       
 *  This file (DecaysExperimentParamsFrame.cxx) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#include <TMath.h>
#include <TPad.h>
#include <TMarker.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TRandom.h>
#include <TSystem.h>
#include <TString.h>
#include <TGLabel.h>
#include <TGDoubleSlider.h>
#include <TGTextEntry.h>
#include <TGLayout.h>

#include "EduKitMain.h"

#include "DecaysClasses.h"

ClassImp(DecaysExperiment)
ClassImp(DecaysExperimentParamsFrame)

//______________________________________________________________________________
DecaysExperiment::DecaysExperiment()
	: EduKitExperiment("decays", "Radioactive decay law & Activity"),
	fParamsFrame(0),
	fAtomsTotalNumber(1000),
	fAtomsRemained(1000),
	fAtomHalfLife(30.0),
	fAtomDecayConstant(0.1),
	fAtoms(0)
{ 	
	fAtomHalfLife = 30.0;
	fAtomsTotalNumber = 400;

	SetDefaults();

	fParamsFrame = new DecaysExperimentParamsFrame(this);
}

DecaysExperiment::~DecaysExperiment() 
{
	
}

EduKitExperimentParamsFrame* DecaysExperiment::GetParamsFrame() 
{ 
	return fParamsFrame; 
}

void DecaysExperiment::SetDefaults()
{
	fAtomsTotalNumber = 400;
	fAtomsRemained = 400;
	fAtomHalfLife = 30.0;

	fSimStepStart = 0;
	fSimStepStep  = 1;
	fSimStepEnd   = 100;
	fSimTimeout   = 33;
}

void DecaysExperiment::Init()
{
	printf("DecaysExperiment::Init() Fetch Current Canvas...\n");
	// make active the current canvas
	TCanvas* pad = gEduKit->GetCanvas();
	
	if(!pad) return;
	pad->Range(0,0, 400,600); // also sets our required canvas size

	fAtomsRemained = fAtomsTotalNumber;
	fAtomDecayConstant = TMath::Log(2.0)/fAtomHalfLife;

	printf("DecaysExperiment::Init() Creating atoms...\n");
	
	fAtoms = new RadAtom_t[fAtomsTotalNumber]; // by default it initializes to 'false'

	Float_t x, y;
	Float_t padW = pad->GetUxmax() - pad->GetUxmin();
	Float_t padH = pad->GetUymax() - pad->GetUymin();

	Float_t gridSpacing =  sqrt(padW*padH/fAtomsTotalNumber);
	int gridMaxColumns = int(padW/gridSpacing);

	x=0.0;
	y=0.0;
	for(int i=0; i<fAtomsTotalNumber; i++)
	{
		x += gridSpacing;

		if((i%gridMaxColumns+1)==gridMaxColumns) {
			x = 0.0;
			y+=gridSpacing;
		}

		//printf("Placing an Atom(%d) at (%f,%f)\n", i, x, y);

		fAtoms[i].sprite = new TMarker(x,y, 6);
		fAtoms[i].sprite->SetMarkerColor(1);
		fAtoms[i].sprite->Draw(); // we need to do this initially in order to add them to the current canvas
		fAtoms[i].decay = false;
	}
	
	printf("DecaysExperiment::Init() Creating Plots...\n");

	// Initialize the output plots 
	//
	// NOTICE: The plots are owned by EduKit, so do not delete them...
	//
	//
	TH1I* histDecays = (TH1I*)gEduKit->NewPlot("TH1I", "histDecays", "Radioactive Decay Experiment");
	histDecays->SetBins(fSimStepEnd/fSimStepStep, 1, fSimStepEnd);
	histDecays->SetMinimum(1);
	histDecays->SetMaximum(fAtomsTotalNumber);
	histDecays->SetStats(0);
	histDecays->SetLineColor(4);
	histDecays->GetXaxis()->SetTitle("time");
	histDecays->GetYaxis()->SetTitle("N particles");
	histDecays->GetYaxis()->SetNdivisions(510);
	histDecays->Draw(); // we need to do this initially in order to add it to the current Plot Canvas

	printf("DecaysExperiment::Init() Showing plots...\n");

	gEduKit->ShowPlot("histDecays");

	printf("DecaysExperiment::Init() Done.\n");
}

void DecaysExperiment::ClearAll()
{
	if(fAtoms){
		// first remove each TMarker from the array
		for(int i=0; i<fAtomsTotalNumber; i++){
			delete fAtoms[i].sprite;
		}

		delete [] fAtoms;
	}
	fAtoms = 0;

	 //
	 // NOTICE: The plots are owned by EduKit, so do not delete them...
	 //
	 //
	 //  do-not-do this: delete fHist; fHist=0;
	 //
	 // Instead, do this:
	 // gEduKit->RemovePlot("histDecays");
	 //
	 // or,
	 //
	 gEduKit->ClearPlots();

	 gEduKit->UpdateDraw();
}

void DecaysExperiment::NextStep(Int_t stepNumber)
{
	TCanvas* mainCanvas = gEduKit->GetCanvas();

	Float_t rGenerated;
	Float_t checkDecayed = fAtomDecayConstant*GetStepStep();
	for(int i=0; i<fAtomsTotalNumber; i++){
		if(fAtoms[i].decay) continue; // jump to next iteration ( Atom)

		rGenerated = gRandom->Rndm();

		if(rGenerated < checkDecayed){
			fAtoms[i].decay = true;
			fAtoms[i].sprite->SetMarkerStyle(29);
			fAtoms[i].sprite->SetMarkerColor(2);
			fAtoms[i].sprite->Draw();

			fAtomsRemained--;
		}

	}

	TH1I* histDecays = (TH1I*)gEduKit->GetPlot("histDecays");

	int idx = TMath::Nint(stepNumber/GetStepStep());

	histDecays->SetBinContent(idx, fAtomsRemained);

	// gSystem->Beep(400, 50);
	gEduKit->UpdateDraw();
}

Int_t DecaysExperiment::GetAtomsNumber() const { return fAtomsTotalNumber; }
Float_t DecaysExperiment::GetAtomsHalfLife() const { return fAtomHalfLife; }

void DecaysExperiment::SetAtomsNumber(Int_t n) { fAtomsTotalNumber = n; }
void DecaysExperiment::SetAtomsHalfLife(Float_t hl) { fAtomHalfLife = hl; } 

/********************************************************************************
 *
 *        GUI  for the Experiments Settings
 *
 *
 *******************************************************************************/

//______________________________________________________________________________
DecaysExperimentParamsFrame::DecaysExperimentParamsFrame()
	: EduKitExperimentParamsFrame( gClient->GetRoot()),
	fExp(0),
	fEntryAtomHalfLife(0),
	fEntryAtomsTotalNumber(0),
	fEntrySimEventsNumber(0)
{
	SetCleanup(kDeepCleanup);

	SetLayoutManager(new TGMatrixLayout(this, 0, 2, 10));

	// Row Half-Life
	TGLabel* labelAtomHL =  new TGLabel(this, "Half-Life:");
	fEntryAtomHalfLife = new TGTextEntry(this, "30");
	AddFrame(labelAtomHL);
	AddFrame(fEntryAtomHalfLife);

	// Row Nucleai Total Number
	TGLabel* labelAtomsNumber =  new TGLabel(this, "Nucleai:");
	fEntryAtomsTotalNumber = new TGTextEntry(this, "400");
	AddFrame(labelAtomsNumber);
	AddFrame(fEntryAtomsTotalNumber);

	// Row Sim events number
	TGLabel* labelSimEventsNumber = new TGLabel(this, "Events:");
	fEntrySimEventsNumber = new TGTextEntry(this, "10");
	AddFrame(labelSimEventsNumber);
	AddFrame(fEntrySimEventsNumber);

	onDefaulted();

	//fEntrySimEventsNumber->Connect("TextChanged(const char*)",  "DecaysExperimentParamsFrame", this, "UpdateTimeStepLabel()");

	Resize();
}

DecaysExperimentParamsFrame::DecaysExperimentParamsFrame(DecaysExperiment* exp)
	: EduKitExperimentParamsFrame( gClient->GetRoot()),
	fExp(exp),
	fEntryAtomHalfLife(0),
	fEntryAtomsTotalNumber(0),
	fEntrySimEventsNumber(0)
{
	SetCleanup(kDeepCleanup);

	SetLayoutManager(new TGMatrixLayout(this, 0, 2, 10));

	// Row Half-Life
	TGLabel* labelAtomHL =  new TGLabel(this, "Half-Life:");
	fEntryAtomHalfLife = new TGTextEntry(this, "30");
	AddFrame(labelAtomHL);
	AddFrame(fEntryAtomHalfLife);

	fEntryAtomHalfLife->Resize(100);

	// Row Nucleai Total Number
	TGLabel* labelAtomsNumber =  new TGLabel(this, "Nucleai:");
	fEntryAtomsTotalNumber = new TGTextEntry(this, "400");
	AddFrame(labelAtomsNumber);
	AddFrame(fEntryAtomsTotalNumber);
	fEntryAtomsTotalNumber->Resize(100);

	// Row Sim events number
	TGLabel* labelSimEventsNumber = new TGLabel(this, "Number of Events:");
	fEntrySimEventsNumber = new TGTextEntry(this, "10");
	AddFrame(labelSimEventsNumber);
	AddFrame(fEntrySimEventsNumber);
	fEntrySimEventsNumber->Resize(100);

	onDefaulted();

	//fEntrySimEventsNumber->Connect("TextChanged(const char*)",  "DecaysExperimentParamsFrame", this, "UpdateTimeStepLabel()");
	

	Resize(200);
}

void DecaysExperimentParamsFrame::onAccepted()
{
	TString nucleaiStr(fEntryAtomsTotalNumber->GetText());
	TString hlStr(fEntryAtomHalfLife->GetText());

	if(fExp){
		// set basic simulation data
		fExp->SetAtomsNumber(nucleaiStr.Atoi());
		fExp->SetAtomsHalfLife(hlStr.Atof());
	
		fExp->SetStepEnd( TString(fEntrySimEventsNumber->GetText()).Atoi() );
	}
}

void DecaysExperimentParamsFrame::onCanceled()
{
	// do nothing
}

void DecaysExperimentParamsFrame::onDefaulted()
{
	if(!fExp) return;

	fExp->SetDefaults();

	LoadData();
}

void DecaysExperimentParamsFrame::LoadData()
{
	if(!fExp) return;

	fEntryAtomHalfLife->SetText( Form("%f", fExp->GetAtomsHalfLife()) );
	fEntryAtomsTotalNumber->SetText( Form("%d", fExp->GetAtomsNumber()) );

	fEntrySimEventsNumber->SetText( Form("%d", fExp->GetStepEnd()) );
}

Float_t DecaysExperimentParamsFrame::GetTimeStep() const
{
	Int_t nevents = TString(fEntrySimEventsNumber->GetText()).Atoi();

	if(nevents<1) return 0.0;

	Float_t maxTime, minTime;
	minTime = fExp->GetStepStart();
	maxTime = fExp->GetStepEnd();

	if(minTime==maxTime) return 0.0;

	Float_t timeStep = (maxTime-minTime)/nevents;

	return timeStep;
}