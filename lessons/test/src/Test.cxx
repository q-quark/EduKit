#include "EduKitMain.h"

#include "Test.h"

ClassImp(TestExperiment)

TestExperiment::TestExperiment() 
	: EduKitExperiment("test", "Acesta este titlul experimentului de TEST")
{

}

TestExperiment::~TestExperiment()
{

}

void TestExperiment::Init()
{

}

void TestExperiment::NextStep(Int_t stepNumber)
{

}

void TestExperiment::ClearAll()
{

}

void TestExperiment::SetDefaults()
{

}