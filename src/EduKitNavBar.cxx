/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitNavBar.cxx) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#include <TGButton.h>
#include <TGPicture.h>

#include "EduKitNavBar.h"

ClassImp(EduKitNavBar)
EduKitNavBar::EduKitNavBar(const TGWindow* p, TGPicturePool* pool)
	: TGHorizontalFrame(p),
	fPool(pool),
	fSettingsButton(0),
	fResetButton(0),
	fPlayButton(0),
	fNextButton(0),
	fInfoButton(0),
	fProgressBar(0)
{
	fSettingsButton = new TGPictureButton(this, pool->GetPicture("button-configure.png"));
	fSettingsButton->ChangeOptions(kChildFrame | kOwnBackground);

	fResetButton = new TGPictureButton(this, pool->GetPicture("button-reset.png"));
	fResetButton->ChangeOptions(kChildFrame | kOwnBackground);

	fPlayButton  = new TGPictureButton(this, pool->GetPicture("button-play.png"));
	fPlayButton->AllowStayDown(kTRUE);
	fPlayButton->ChangeOptions(kChildFrame | kOwnBackground);

	fNextButton  = new TGPictureButton(this, pool->GetPicture("button-next.png"));
	fNextButton->ChangeOptions(kChildFrame | kOwnBackground);
	
	fInfoButton  = new TGPictureButton(this, pool->GetPicture("button-info.png"));
	fInfoButton->ChangeOptions(kChildFrame | kOwnBackground);
	//fInfoButton->AllowStayDown(kTRUE);

	fProgressBar = new TGHProgressBar(this, TGProgressBar::kFancy, 100);
	fProgressBar->SetRange(0.0, 100.0);
	fProgressBar->SetPosition(0.0);
	fProgressBar->SetBarColor("blue");
	//fProgressBar->SetFillType(TGProgressBar::kBlockFill);
	//fProgressBar->SetForegroundColor(2);
	fProgressBar->Format("Event: %f");
	fProgressBar->ShowPosition();

	AddFrame(fSettingsButton, new TGLayoutHints(kLHintsLeft | kLHintsCenterY) );
	AddFrame(fResetButton, new TGLayoutHints(kLHintsLeft | kLHintsCenterY) );
	AddFrame(fPlayButton, new TGLayoutHints(kLHintsLeft | kLHintsCenterY) );
	AddFrame(fNextButton, new TGLayoutHints(kLHintsLeft | kLHintsCenterY) );
	AddFrame(fProgressBar, new TGLayoutHints(kLHintsLeft | kLHintsCenterY | kLHintsExpandX) );
	AddFrame(fInfoButton, new TGLayoutHints(kLHintsRight | kLHintsCenterY) );

	fSettingsButton->Connect("Released()","EduKitNavBar", this, "Settings()");
	fResetButton->Connect("Released()","EduKitNavBar", this, "Reset()");
	fPlayButton->Connect("Clicked()", "EduKitNavBar", this, "onPlayToggled()");
	fNextButton->Connect("Released()", "EduKitNavBar", this, "Next()");
	fInfoButton->Connect("Released()", "EduKitNavBar", this, "Info()");

	MapSubwindows();
	MapWindow();
	Layout();
}

EduKitNavBar::~EduKitNavBar()
{
	
}

void EduKitNavBar::onPlayToggled()
{
	if(fPlayButton->IsDown()) {
		fPlayButton->SetPicture(fPool->GetPicture("button-pause.png"));
		Play();
	}
	else {
		fPlayButton->SetPicture(fPool->GetPicture("button-play.png"));
		Pause();
	}
}

void EduKitNavBar::Settings(){ Emit("Settings()"); }
void EduKitNavBar::Reset(){ Emit("Reset()"); }
void EduKitNavBar::Play() { Emit("Play()"); }
void EduKitNavBar::Pause() { Emit("Pause()"); }
void EduKitNavBar::Next() { Emit("Next()"); }
void EduKitNavBar::Info() { Emit("Info()"); }