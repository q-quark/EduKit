/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitPlotWindow.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
 
#ifndef _EDUKIT_PLOTWINDOW_H_
#define _EDUKIT_PLOTWINDOW_H_

#include <TGFrame.h>

class TObject;
class TCanvas;
class TRootEmbeddedCanvas;

class EduKitPlotWindow : public TGTransientFrame
{
public:
	EduKitPlotWindow(const char* name, const TGWindow* parent, UInt_t width, UInt_t height);
	~EduKitPlotWindow();
	
	TCanvas* GetCanvas() const; // get drawing area
	void* GetPlot() const; // get the plot

	void Show();
	void Hide(); // *SIGNAL*
protected:
	TRootEmbeddedCanvas* fCanvasWidget;
	void* fPlotObject; // this may be any plotting object (TH1, TH2, TGraph, TGraphErrors, etc..)

	friend class EduKitPlotManager;

public:
	ClassDef(EduKitPlotWindow, 0)
};

#endif