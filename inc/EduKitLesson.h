/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitLesson.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#ifndef _EDUKIT_LESSON_H_
#define _EDUKIT_LESSON_H_

#include <TString.h>
#include <TNamed.h>

class EduKitExperiment;

class EduKitLesson : public TNamed
{
public:
	EduKitLesson(const char* name); // name of the lesson (same as the directory name)
	~EduKitLesson();

	const char* GetDescription() const;
	const char* GetExperimentClass() const; // class that implements EduKitExperiment
	const char* GetExperimentFile() const; // the implemenation file (.cxx) that implements the experiment
	const char* GetManual() const;

	const char* GetLessonDir() const; // returns the full path to the directory of this lesson 
	const char* GetResourceDir() const; // returns the full path to the resources ("res") directory of this lesson 
	const char* GetDocDir() const; // returns the full path to the documentation ("doc") directory of this lesson

	void SetDescription(const char* description);
	void SetExperimentClass(const char* experimentClassName);
	void SetExperimentFile(const char* experimentFileName);
	void SetManual(const char* manualFileName);

	Bool_t ReadFile(const char* fileName);
	void WriteFile(const char* fileName);
	
	static EduKitLesson*  LoadFromFile(const char* fileName);

protected:
	TString fDescription; // short description of the lesson
	TString fExperimentClassName; // the experiments class name
	TString fExperimentFileName;
	TString fManualFileName;

public:
	ClassDef(EduKitLesson,0)
};

#endif