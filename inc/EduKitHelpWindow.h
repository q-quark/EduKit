/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitHelpWindow.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
 
#ifndef _EDUKIT_HELPWINDOW_H_
#define _EDUKIT_HELPWINDOW_H_

#include <TGFrame.h>

class TGHtml;

class EduKitHelpWindow : public TGTransientFrame
{
public:
	EduKitHelpWindow(const TGWindow* parent, UInt_t width=600, UInt_t height=400);
	virtual ~EduKitHelpWindow();

	Bool_t LoadFile(const char* filePath);

	void Show();
	void Hide();

protected:
	TGHtml* fView;
	TString fFileName;

public:
	ClassDef(EduKitHelpWindow,0)
};

#endif