#ifndef EXAMPLE_CLASSES_H__
#define EXAMPLE_CLASSES_H__ 

#include <string>

#include "EduKitExperiment.h"
#include "EduKitExperimentParamsFrame.h"

class TGLabel;
class TGTextEntry;
class TMarker;
class TArrow;
class TText;
class TLatex;

class EduKitNucleus;

class AlphaDecayExperimentParamsFrame;

class alphadecay : public EduKitExperiment
{
public:
	// MUST have at least one default constructor
	alphadecay();
	~alphadecay();

	void Init();
	void NextStep(Int_t stepNumber);
	void ClearAll();

	void SetDefaults();

	// return nuclei mother properties
	Int_t GetA() const;
	Int_t GetZ() const;

	// Other methods
    void SetParams(Int_t A, Int_t Z);
    EduKitExperimentParamsFrame* GetParamsFrame();
protected:
	// Here put your class variable members	
	void drawInfoPad_SCM();

	//temporary variables
	Int_t	fMotherA;
	Int_t	fMotherZ;

	EduKitNucleus *fParticleMother;
	EduKitNucleus *fParticleDaughter;

	TMarker* fMotherMarker;
	TMarker* fDaughterMarker;
	TMarker* fAlphaMarker;

	TText* fMotherText;
	TText* fDaughterText;
	TText* fAlphaText;

	TArrow* fDaughterArrow;
	TArrow* fAlphaArrow;

	// GUI Window to setup the simulation variables
	AlphaDecayExperimentParamsFrame* fParamsFrame;

	// SCM PAD
	TText* fSCMText;
	TLatex* fSCMReactionText;
	TLatex* fSCMSText;
	TLatex* fSCMEalphaText;
	TLatex* fSCMEdaughterText;
	TLatex* fSCMPalphaText;
	TLatex* fSCMAngleText;
public:
	ClassDef(alphadecay, 0)
};


class AlphaDecayExperimentParamsFrame : public EduKitExperimentParamsFrame
{
public:
	AlphaDecayExperimentParamsFrame();
	AlphaDecayExperimentParamsFrame(alphadecay* exp);
	~AlphaDecayExperimentParamsFrame(){}

	void onAccepted();
	void onCanceled();
	void onDefaulted();	

protected:
	void LoadData(); // loads all data and set it to GUI objects

	alphadecay* fExp; // parent experiment
	TGTextEntry* fEntryA;
	TGTextEntry* fEntryZ;
public:
	ClassDef(AlphaDecayExperimentParamsFrame, 0)
};
#endif