/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitSimEngine.cxx) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
 
#include <TTimer.h>

#include "EduKitExperiment.h"
#include "EduKitSimEngine.h"


EduKitSimEngine::EduKitSimEngine()
	: fTimer(0),
	fExperiment(0),
	fTimeStart(0.0),
	fTimeStep(0.0),
	fTimeMax(0.0),
	fTimeCur(0.0),
	fRefreshTimeOut(33),
	fIsRunning(kFALSE)
{
	fTimer = new TTimer;
	fTimer->Connect("Timeout()", "EduKitSimEngine", this, "NextStep()");
}

EduKitSimEngine::~EduKitSimEngine()
{
	if(fExperiment) delete fExperiment;	fExperiment=0;

	delete fTimer; fTimer=0;
}

void EduKitSimEngine::Init()
{
	if(!fExperiment) return;

	fTimeStart = fExperiment->GetStepStart();
	fTimeStep  = fExperiment->GetStepStep();
	fTimeMax   = fExperiment->GetStepEnd();
	fRefreshTimeOut = fExperiment->GetTimeout();

	fTimeCur = fTimeStart;

	fExperiment->Init();
}

void EduKitSimEngine::Run()
{
	if(fIsRunning) return;
	fIsRunning = kTRUE;

	fTimer->Start(fRefreshTimeOut, kFALSE);

	Starting();
}

void EduKitSimEngine::Stop()
{
	printf("Stopping Simulation...\n");

	fTimer->Stop();
	fIsRunning = kFALSE;

	Stopped();
}

void EduKitSimEngine::NextStep()
{
	StepEntering();

// comment this to allow step-by-step  Simulation advancing
//	if(!fTimer->HasTimedOut()) return;

	if( fTimeCur >= fTimeMax ) { 
		Stop(); 
		Finished(); 
		return;
	}

	printf("SimEngine Step %d\n", fTimeCur);

	if(fExperiment) fExperiment->NextStep(fTimeCur);

	StepFinished();
	
	fTimeCur+=fTimeStep;
}

void EduKitSimEngine::Clear()
{
	if(fExperiment) fExperiment->ClearAll();
}

void EduKitSimEngine::Reset()
{
	Stop();
	Clear();
	Init();
}

void EduKitSimEngine::RegisterExperiment(EduKitExperiment* exp)
{
	if(!exp) return;
	
	//UnregisterExperiment();
	
	fExperiment = exp;
}

void EduKitSimEngine::UnregisterExperiment()
{
	if(fExperiment)
	{
		delete fExperiment; fExperiment=0;
	}

	fTimeStart = 0;
	fTimeStep  = 0;
	fTimeMax   = 0;
	fRefreshTimeOut = 0;

	fTimeCur = 0;	
}

Bool_t EduKitSimEngine::IsRunning() const
{
	return fIsRunning;
}

EduKitExperiment* EduKitSimEngine::GetExperiment() const
{
	return fExperiment;
}

void EduKitSimEngine::Starting()
{
	Emit("Starting()");
}

void EduKitSimEngine::StepEntering()
{
	Emit("StepEntering()");
}

void EduKitSimEngine::StepFinished()
{
	Emit("StepFinished()");
}

void EduKitSimEngine::Stopped()
{
	printf("Stopped Simulation...\n");

	Emit("Stopped()");
}

void EduKitSimEngine::Finished()
{
	Emit("Finished()");
}