/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitSimEngine.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/


#ifndef _EDUKIT_SIMENGINE_H_
#define _EDUKIT_SIMENGINE_H_ 

#include <RQ_OBJECT.h>

class TTimer;
class EduKitExperiment;

class EduKitSimEngine
{
	RQ_OBJECT("EduKitSimEngine")

public:

	EduKitSimEngine();
	virtual ~EduKitSimEngine();
	
	void Init();
	void Run();
	void Stop();  // pause the simulation
	void NextStep();
	void Clear(); // stop the simulation & deallocate everything allocated in Init()
	void Reset(); // start the simulation from begining

	Bool_t IsRunning() const;
	
	void RegisterExperiment(EduKitExperiment* exp);
	void UnregisterExperiment();

	EduKitExperiment* GetExperiment() const;

	void Starting(); // *SIGNAL*
	void StepEntering(); // *SIGNAL*
	void StepFinished(); // *SIGNAL*
	void Stopped(); // *SIGNAL*
	void Finished(); // *SIGNAL*


	inline Int_t GetCurTime() const { return fTimeCur; }
protected:
	TTimer *fTimer;
	EduKitExperiment* fExperiment;

	Int_t fTimeStart; // simulation start index
	Int_t fTimeStep;  // simulation step size
	Int_t fTimeMax;  // max simulation step
	Long_t fRefreshTimeOut;

	Int_t fTimeCur; // current step index
	bool  fIsRunning;
};

#endif