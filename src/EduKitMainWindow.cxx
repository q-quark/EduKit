/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitMainWindow.cxx) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
#include <fstream>

#include <TSystem.h>
#include <TClass.h>
#include <TROOT.h>

#include <TCanvas.h>
#include <TGPicture.h>
#include <TRootEmbeddedCanvas.h>
#include <TGMenu.h>
#include <TGHtml.h>
#include <TGMsgBox.h>


#include "EduKitNucleusInfoDB.h"
#include "EduKitUtils.h"
#include "EduKitExperiment.h"
#include "EduKitSimEngine.h"
#include "EduKitNavBar.h"
#include "EduKitPlotManager.h"
#include "EduKitInfoWidget.h"
#include "EduKitInputParamsWindow.h"
#include "EduKitLesson.h"
#include "EduKitExperimentParamsFrame.h"
#include "EduKitHelpWindow.h"
#include "EduKitLessonLoaderWindow.h"

#include "EduKitMainWindow.h"

enum EEK_MENU_ID{
	// File Menu
	kEK_LESSON_NEW,
	kEK_LESSON_OPEN,
	kEK_LESSON_SAVE,
	kEK_LESSON_CLOSE,
	kEK_LESSON_QUIT,

	// Simulation Menu
	kEK_SIMULATION_PLAY,
	kEK_SIMULATION_PAUSE,
	kEK_SIMULATION_NEXTSTEP,
	kEK_SIMULATION_RESET,

	// Plots Menu
	// ... no need since it is populated dynamically

	// Help Menu
	kEK_HELP_LESSON,
	kEK_HELP_HELP,
	kEK_HELP_ABOUT
};


ClassImp(EduKitMainWindow)

EduKitMainWindow::EduKitMainWindow(const TGWindow* p, int width, int height)
	: TGMainFrame(p, width, height, kVerticalFrame),
	fPicturePool(0),
	fMenuBar(0),
	fMenuFile(0),
	fMenuSimulation(0),
	fMenuPlots(0),
	fMenuHelp(0),
	fAnimationWidget(0),
	fNavBar(0),
	fInfoWidget(0),
	fParamsWindow(0),
	fHtmlViewer(0),
	fPlotManager(0),
	fSimEngine(0),
	fCurrentLesson(0),
	fLessonLoaderWindow(0),
	fNucleaiDB(0)
{
	fBaseIncludePath = gSystem->GetIncludePath();

	fPicturePool = new TGPicturePool(p->GetClient(), Form("%s/res/icons", gSystem->Getenv("EDUKIT_PATH")) );

	SetupMenu();

	fAnimationWidget = new TRootEmbeddedCanvas("animationWdiget", this, width/2.0, height/2.0);
	AddFrame(fAnimationWidget, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY) );

	fNavBar = new EduKitNavBar(this, fPicturePool);
	AddFrame(fNavBar, new TGLayoutHints(kLHintsExpandX) );

	fInfoWidget = new EduKitInfoWidget(this);
	AddFrame(fInfoWidget, new TGLayoutHints(kLHintsExpandX) );



	fPlotManager = new EduKitPlotManager(fMenuPlots);
	
	fSimEngine = new EduKitSimEngine();

	fHtmlViewer = new EduKitHelpWindow(this, 600, 400);

	fLessonLoaderWindow = new EduKitLessonLoaderWindow(gClient->GetRoot(), this, 600, 400);
	//fLessonLoaderWindow->UnmapWindow();

	fNucleaiDB = new EduKitNucleusInfoDB( Form("%s/res/nucleai.dat", gSystem->Getenv("EDUKIT_PATH")) );


	fLessonLoaderWindow->Connect("Accepted(EduKitLesson*)", "EduKitMainWindow", this, "onAcceptedLoadLesson(EduKitLesson*)");

	fNavBar->Connect("Reset()", "EduKitMainWindow", this, "SimReset()");
	fNavBar->Connect("Play()", "EduKitSimEngine", fSimEngine, "Run()");
	fNavBar->Connect("Pause()", "EduKitSimEngine", fSimEngine, "Stop()");
	fNavBar->Connect("Next()", "EduKitSimEngine", fSimEngine, "NextStep()");
	fNavBar->Connect("Info()", "EduKitMainWindow", this, "ShowInfo()");

	fSimEngine->Connect("StepEntering()", "EduKitMainWindow", this, "onSimStepEntering()");
	fSimEngine->Connect("StepFinished()", "EduKitMainWindow", this, "onSimStepFinished()");
	//fSimEngine->Connect("Stopped()", "EduKitMainWindow", this, "onSimStepFinished()");

	Resize(800,600);

	MapSubwindows();
	MapWindow();

	HideFrame(fInfoWidget);

	Layout();

	fHtmlViewer->Hide();

	gROOT->Reset();
}

EduKitMainWindow::~EduKitMainWindow()
{
	UnLoadCurrentLesson();

	delete fNucleaiDB; fNucleaiDB = 0;
	delete fPlotManager; fPlotManager = 0;
	delete fPicturePool; fPicturePool = 0;
}

TGPicturePool* EduKitMainWindow::GetPicturePool() const
{
	return fPicturePool;
}

void EduKitMainWindow::SetupMenu()
{
	fMenuBar = new TGMenuBar(this);
	AddFrame(fMenuBar, new TGLayoutHints(kLHintsExpandX));

	// File Menu
	fMenuFile = new TGPopupMenu(gClient->GetRoot());
	//fMenuFile->AddEntry("New", kEK_LESSON_NEW);
	fMenuFile->AddEntry("Load Lesson",kEK_LESSON_OPEN);
	//fMenuFile->AddEntry("Save",kEK_LESSON_SAVE);
	fMenuFile->AddEntry("Close",kEK_LESSON_CLOSE);
	fMenuFile->AddSeparator();
	fMenuFile->AddEntry("Quit EduKit",kEK_LESSON_QUIT);

	// Simulation Menu
	// fMenuSimulation = new TGPopupMenu(gClient->GetRoot());
	// fMenuSimulation->AddEntry("Play", kEK_SIMULATION_PLAY);
	// fMenuSimulation->AddEntry("Pause",kEK_SIMULATION_PAUSE);
	// fMenuSimulation->AddEntry("Next Step",kEK_SIMULATION_NEXTSTEP);
	// fMenuSimulation->AddSeparator();
	// fMenuSimulation->AddEntry("Reset",kEK_SIMULATION_RESET);

	fMenuPlots = new TGPopupMenu(gClient->GetRoot());
	// Plots Menu doesnt have any, since its populated dynamically

	// Help Menu
	fMenuHelp = new TGPopupMenu(gClient->GetRoot());
	fMenuHelp->AddEntry("Lesson Walkthrough", kEK_HELP_LESSON);
	//fMenuHelp->AddEntry("Help", kEK_HELP_HELP);
	fMenuHelp->AddEntry("About",kEK_HELP_ABOUT);

	fMenuBar->AddPopup("Lesson", fMenuFile, new TGLayoutHints(kLHintsNormal));
	//fMenuBar->AddPopup("Simulation", fMenuSimulation, new TGLayoutHints(kLHintsNormal));
	fMenuBar->AddPopup("Plots", fMenuPlots, new TGLayoutHints(kLHintsNormal));
	fMenuBar->AddPopup("Help", fMenuHelp, new TGLayoutHints(kLHintsRight | kLHintsTop));

	fMenuFile->Connect("Activated(Int_t)", "EduKitMainWindow", this, "onMenuActivated(Int_t)");
	//fMenuSimulation->Connect("Activated(Int_t)", "EduKitMainWindow", this, "onMenuActivated(Int_t)");
	fMenuPlots->Connect("Activated(Int_t)", "EduKitMainWindow", this, "onMenuPlotActivated(Int_t)");
	fMenuHelp->Connect("Activated(Int_t)", "EduKitMainWindow", this, "onMenuActivated(Int_t)");

}

void EduKitMainWindow::onMenuActivated(Int_t id)
{
	switch(id)
	{
	case kEK_LESSON_OPEN:
	{
		UnmapWindow();
		fPlotManager->HideAllPlots();
		
		fLessonLoaderWindow->MapWindow();
		
		MapWindow();
		fPlotManager->ShowAllPlots();
		break;
	}
	case kEK_LESSON_CLOSE:
	{
		UnLoadCurrentLesson();
		break;
	}
	case kEK_LESSON_QUIT:
	{
		SendCloseMessage();
		break;
	}
	case kEK_SIMULATION_PLAY:
	{
		break;
	}
	case kEK_HELP_LESSON:{
		ShowLessonHelp();
		break;
	}
	case kEK_HELP_ABOUT:{
		ShowAboutDialog();
		break;
	}
	default:{
		// do nothing at all
	}
	}
}

void EduKitMainWindow::onMenuPlotActivated(Int_t id)
{
	TGMenuEntry* plotMenuEntry = fMenuPlots->GetEntry(id);
	
	if(!plotMenuEntry) return;

	const char *name = (const char *)plotMenuEntry->GetUserData();

	if(fMenuPlots->IsEntryChecked(id)){
		fPlotManager->HidePlot(name);
	}
	else {
		fPlotManager->ShowPlot(name);
	}
}

bool EduKitMainWindow::LoadLesson(EduKitLesson* lesson)
{
	printf("EduKitMainWindow::LoadLesson() \n");
	if(lesson==0) return kFALSE;

	if(fCurrentLesson) UnLoadCurrentLesson();

	fCurrentLesson = lesson;

	SetWindowName( Form("%s - EduKit", fCurrentLesson->GetTitle()) );

	fInfoWidget->SetText(fCurrentLesson->GetDescription());

	// load the experiment for this lesson

	const char* lessonDir = Form("%s/lessons/%s", gSystem->Getenv("EDUKIT_PATH"), lesson->GetName());
	const char* experimentDir = Form("%s/src", lessonDir);

	printf("EduKitMainWindow::LoadLesson() LessonDir:%s ExperimentDir:%s\n", lessonDir, experimentDir);
	printf("EduKitMainWindow::LoadLesson() - Current IncludePath: [%s]\n", gSystem->GetIncludePath() );

	const char* experimentFile = Form("%s/%s+", experimentDir, lesson->GetExperimentFile());
	

	printf("EduKitMainWindow::LoadLesson() Loading Experiment File: %s ...\n", experimentFile);
	
	EduKitUtils::LoadMacro(experimentFile);
	printf("EduKitMainWindow::LoadLesson() Loaded. Getting Experiment Class ...\n");

	TClass *cl = TClass::GetClass(lesson->GetExperimentClass());

	if(!cl || !cl->InheritsFrom("EduKitExperiment")) return kFALSE;

	printf("EduKitMainWindow::LoadLesson() Class %s at %p Loaded!... \n", cl->Class_Name(), cl);

	printf("EduKitMainWindow::LoadLesson() Creating Experiment... \n");
	void* expPtr = cl->New();
	
	printf("EduKitMainWindow::LoadLesson() Experiment allocated. Dumping it to terminal... \n");

	cl->Dump(expPtr);

	EduKitExperiment* exp = (EduKitExperiment*)cl->DynamicCast(EduKitExperiment::Class(), expPtr, kTRUE);

	printf("EduKitMainWindow::LoadLesson() Created object of Class_Name %s at %p... \n", exp->Class_Name(), exp);

	printf("EduKitMainWindow::LoadLesson() Registering Experiment... \n");
	fSimEngine->RegisterExperiment(exp);

	printf("EduKitMainWindow::LoadLesson() Init Experiment... \n");
	fSimEngine->Init();

	printf("EduKitMainWindow::LoadLesson() Adjusting progressBar... \n");

	fNavBar->SetProgressRange(exp->GetStepStart(), exp->GetStepEnd());
	fNavBar->SetProgressPosition(exp->GetStepStart());

	printf("EduKitMainWindow::LoadLesson() Setting Settings Window... \n");
	fParamsWindow = new EduKitInputParamsWindow(gClient->GetRoot(), this, exp->GetParamsFrame(), 250,200);

	fParamsWindow->Connect("Accepted()", "EduKitMainWindow", this, "onAcceptedSimSettings()");
	fNavBar->Connect("Settings()", "EduKitInputParamsWindow", fParamsWindow, "Show()");

	printf("EduKitMainWindow::LoadLesson() Done! \n");

	return kTRUE;
}

void EduKitMainWindow::UnLoadCurrentLesson()
{
	if(!fCurrentLesson) return;

	const char* experimentClassName = fCurrentLesson->GetExperimentClass();

	fSimEngine->Stop();
	fSimEngine->Clear();

	// DONOT delete - the lesson is not owned by MainWindow
	//delete fCurrentLesson;
	fCurrentLesson = 0;

	fNavBar->Disconnect("Settings()", fParamsWindow, "Show()");

	fParamsWindow->SendCloseMessage();
	delete fParamsWindow;
	fParamsWindow = 0;


	fPlotManager->Clear();

	TCanvas* mainCanvas = fAnimationWidget->GetCanvas();
	mainCanvas->Clear();

	fNavBar->SetProgressRange(0, 0);
	fNavBar->SetProgressPosition(0);

	fSimEngine->UnregisterExperiment();

	gSystem->Unload(experimentClassName);

	gROOT->Reset();
}

void EduKitMainWindow::ShowLessonHelp()
{
	if(!fCurrentLesson) return;

	const char* lessonDir = Form("%s/lessons/%s", gSystem->Getenv("EDUKIT_PATH"), fCurrentLesson->GetName());
	const char* htmlFilePath = Form("%s/doc/%s", lessonDir, fCurrentLesson->GetManual() );

	if(!fHtmlViewer->LoadFile(htmlFilePath)){
		new TGMsgBox(gClient->GetRoot(), this,
                "EduKit Error!", "Couldn't Open the Walkthrough Documentation for this Lesson!",
                kMBIconExclamation, kMBOk);

		return;
	}

	fHtmlViewer->Show();

}

void EduKitMainWindow::ShowAboutDialog()
{
	std::fstream AuthorsFileH;
	AuthorsFileH.open(Form("%s/AUTHORS", gSystem->Getenv("EDUKIT_PATH")) );

	if(!AuthorsFileH.is_open()) {
		printf("Couldn't read the AUTHORS file !!!\n" );
	}

	TString aboutString;
	aboutString.ReadFile(AuthorsFileH);

	AuthorsFileH.close();

	new TGMsgBox(gClient->GetRoot(), this,
                "About EduKit", aboutString.Data(),
                kMBIconExclamation, kMBOk);
}

void EduKitMainWindow::ShowInfo()
{
	if(!fInfoWidget->IsMapped()) 
		ShowFrame(fInfoWidget);
	else
		HideFrame(fInfoWidget);
}

void EduKitMainWindow::SimReset()
{
	fSimEngine->Reset();
	EduKitExperiment* exp = fSimEngine->GetExperiment();

	if(exp){
		fNavBar->SetProgressRange(exp->GetStepStart(), exp->GetStepEnd());
		fNavBar->SetProgressPosition(exp->GetStepStart());
	}
}

void EduKitMainWindow::onSimStepEntering()
{

}

void EduKitMainWindow::onSimStepFinished()
{	
	fNavBar->SetProgressPosition( fSimEngine->GetCurTime() );
}

void EduKitMainWindow::onShowSimSettings()
{	
	fSimEngine->Stop();
	
	if(fParamsWindow) fParamsWindow->Show();
}

void EduKitMainWindow::onAcceptedSimSettings()
{
	fSimEngine->Stop();
	fSimEngine->Clear();
	
	if(fParamsWindow && fParamsWindow->GetParamsFrame()) fParamsWindow->GetParamsFrame()->onAccepted();
	
	fSimEngine->Init();

	EduKitExperiment* exp = fSimEngine->GetExperiment();

	if(exp){
		fNavBar->SetProgressRange(exp->GetStepStart(), exp->GetStepEnd());
		fNavBar->SetProgressPosition(exp->GetStepStart());
	}
}

void EduKitMainWindow::onAcceptedLoadLesson(EduKitLesson* lesson)
{
	if(lesson==0) return;

	UnLoadCurrentLesson();

	if(!LoadLesson(lesson))
	{
		new TGMsgBox(gClient->GetRoot(), this,
                "EduKit Error!", Form("Couldn't Load Lesson %s !", lesson->GetTitle() ),
                kMBIconExclamation, kMBOk);

		return;
	}
}