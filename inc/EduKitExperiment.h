/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitExperiment.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#ifndef _EDUKIT_EXPERIMENT_H_
#define _EDUKIT_EXPERIMENT_H_

#include <TNamed.h>

class EduKitExperimentParamsFrame;

class EduKitExperiment : public TNamed
{
protected:
	// Prefered simulation settings
	Int_t fSimStepStart;
	Int_t fSimStepStep;
	Int_t fSimStepEnd;
	Long_t fSimTimeout;

public:
	EduKitExperiment(const char* name = "GenericExperiment", const char* title = "Generic Experiment");
	virtual ~EduKitExperiment();

	// MUST OVERIDE THESE METHODS IN YOUR THE DERIVED CLASSES
	virtual void Init(); // allocate here your objects (objects, plots, etc...)
	virtual void NextStep(Int_t stepNumber); // do just a single simulation step
	virtual void ClearAll(); // deallocate (free memory) everything allocated in Init()
	
	// Optional OVERIDE these methods
	virtual void SetDefaults();

	// return the name of the class name that implements the GUI that sets up the experiment's input parameters
	// it MUST derive directly from TGGroupFrame
	virtual EduKitExperimentParamsFrame* GetParamsFrame();

	// Prefered Settings for this experiment
	virtual Int_t GetStepStart() const; // returns the initial start Step of the simulation
	virtual Int_t GetStepStep() const; // returns the Step step of the simulation
	virtual Int_t GetStepEnd() const;  // returns the end (maximum) Step of the simulation
	virtual Long_t GetTimeout() const;

	virtual void SetStepStart(Int_t tmin);
	virtual void SetStepStep(Int_t tstep);
	virtual void SetStepEnd(Int_t tmax);
	virtual void SetTimeout(Long_t timeout);

	ClassDef(EduKitExperiment,0)
};

#endif