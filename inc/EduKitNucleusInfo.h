/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitExperiment.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#ifndef _EDUKIT_NUCLEUS_INFO_H_
#define _EDUKIT_NUCLEUS_INFO_H_

#include <string>
#include <Rtypes.h>

class EduKitNucleusInfo
{
public:

	EduKitNucleusInfo();

 	~EduKitNucleusInfo();

	int id;
	int A;
	int Z;
	std::string Name;
	std::string Symbol;
	Double_t MassExcess; // in MeV
	Double_t Mass; // in MeV
	float Charge;

	ClassDef(EduKitNucleusInfo,0)
};

#endif
 