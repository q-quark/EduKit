/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitNavBar.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
 
#ifndef _EDUKIT_NAVBAR_H_
#define _EDUKIT_NAVBAR_H_

#include <RQ_OBJECT.h>
#include <TGFrame.h>
#include <TGProgressBar.h>

class TGPicturePool;
class TGPictureButton;
class TGHProgressBar;

class EduKitNavBar : public TGHorizontalFrame
{
	RQ_OBJECT("EduKitNavBar")

public:
	EduKitNavBar(const TGWindow* p, TGPicturePool* picPool);
	virtual ~EduKitNavBar();

	void Settings();// *SIGNAL*
	void Reset();// *SIGNAL*
	void Play(); // *SIGNAL*
	void Pause();// *SIGNAL*
	void Next(); // *SIGNAL*
	void Info(); // *SIGNAL*

	void onPlayToggled();

	void SetProgressRange(Float_t min, Float_t max) { fProgressBar->SetRange(min,max); }
	
	inline void SetProgressPosition(Float_t pos) 
	{
		fProgressBar->SetPosition(pos);
	}

protected:
	TGPicturePool* fPool;
	TGPictureButton*	fSettingsButton;
	TGPictureButton*	fResetButton;
	TGPictureButton*	fPlayButton;
	TGPictureButton*	fNextButton;
	TGPictureButton*	fInfoButton;
	TGHProgressBar*		fProgressBar;

public:
	ClassDef(EduKitNavBar,0)
};
#endif