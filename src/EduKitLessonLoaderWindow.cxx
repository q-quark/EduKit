#include <TGButton.h>
#include <TGListBox.h>
#include <TGTextView.h>
#include <TGLabel.h>

#include <TInterpreter.h>
#include <TList.h>
#include <TSystem.h>
#include <TSystemFile.h>
#include <TSystemDirectory.h>
#include <TString.h>

#include <TObject.h>

#include "EduKitLessonLoaderWindow.h"
#include "EduKitLesson.h"

ClassImp(EduKitLessonLoaderWindow)
EduKitLessonLoaderWindow::EduKitLessonLoaderWindow(const TGWindow* p, const TGWindow* main, UInt_t width, UInt_t height)
	: TGTransientFrame(p, main, width, height,  kVerticalFrame),
	fListLessonBox(0),
	fButtonCancelLoadLesson(0),
	fButtonLoadLesson(0),
	fTextViewLessonInfo(0)
{
	SetCleanup(kDeepCleanup);

	fListLessons = new TList;
	fListLessons->SetOwner(kTRUE);

	TGHorizontalFrame* hf = new TGHorizontalFrame(this);
	AddFrame(hf, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY) );

	fListLessonBox = new TGListBox(hf);
	fListLessonBox->SetMinWidth(200);
	fListLessonBox->SetMaxWidth(250);

	fTextViewLessonInfo = new TGLabel(hf);

	hf->AddFrame(fListLessonBox, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY) );
	hf->AddFrame(fTextViewLessonInfo, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY) );


	TGHorizontalFrame* hf2 = new TGHorizontalFrame(this);
	AddFrame(hf2, new TGLayoutHints(kLHintsExpandX) );

	fButtonCancelLoadLesson = new TGTextButton(hf2, "Cancel");
	hf2->AddFrame(fButtonCancelLoadLesson, new TGLayoutHints(kLHintsLeft) );

	fButtonLoadLesson = new TGTextButton(hf2, "Load Lesson");
	hf2->AddFrame(fButtonLoadLesson, new TGLayoutHints(kLHintsRight) );

	AddInput(kKeyPressMask);

	SetWindowName("EduKit - Available Lessons");
	SetIconName("EduKit");
	SetClassHints("ROOT", "EduKitLessonLoaderWindow");
	
	SetWMSize(width, height);
	SetWMSizeHints(width, height, width, height, 0, 0);

	SetMWMHints(kMWMDecorTitle | kMWMDecorBorder,  kMWMFuncMove,
			kMWMInputModeless | kMWMInputPrimaryApplicationModal );


	// Manage SIGNALS
	fButtonCancelLoadLesson->Connect("Clicked()", "EduKitLessonLoaderWindow", this, "UnmapWindow()");
	fButtonLoadLesson->Connect("Clicked()", "EduKitLessonLoaderWindow", this, "OnAccepted()");
	fListLessonBox->Connect("Selected(Int_t)", "EduKitLessonLoaderWindow", this, "OnLessonSelected(Int_t)");
	fListLessonBox->Connect("DoubleClicked(Int_t)", "EduKitLessonLoaderWindow", this, "OnAccepted()");

	Connect("CloseWindow()", "EduKitLessonLoaderWindow", this, "UnmapWindow()");

	PopulateLessonList();
}

EduKitLessonLoaderWindow::~EduKitLessonLoaderWindow()
{ }

void EduKitLessonLoaderWindow::MapWindow()
{
	
	MapSubwindows();
	//TGTransientFrame::MapWindow();
	Layout();
	
	CenterOnParent();

	MapRaised();

	fClient->WaitFor(this);
}

void EduKitLessonLoaderWindow::UnmapWindow()
{
	DontCallClose();
	Layout();

	TGTransientFrame::UnmapWindow();

	fClient->ResetWaitFor(this);
}


void EduKitLessonLoaderWindow::PopulateLessonList()
{
	ClearLessons();

	//const char* lessonDirPath =  Form( "%s/lessons",  gSystem->Getenv("EDUKIT_PATH") );
	TSystemDirectory lessonDir( "lessons/", 
		Form("%s/lessons", gSystem->Getenv("EDUKIT_PATH") ));

	TList* listOfFiles = lessonDir.GetListOfFiles();

	TSystemFile* curFile=0;
	TSystemDirectory* curSubDir=0;
	char* filePath = 0;
	EduKitLesson* curLesson = 0;
	for(int i=0; i<listOfFiles->GetEntries(); i++)
	{	
		curFile = (TSystemFile*)listOfFiles->At(i);
		if(curFile->IsDirectory() && strcmp(curFile->GetName(),".")!=0 && strcmp(curFile->GetName(),"..")!=0)
		{
			filePath =  Form("%s/lessons/%s/lesson.conf", gSystem->Getenv("EDUKIT_PATH") , curFile->GetName());
			printf("Loading Lesson From File: %s\n", filePath);
			if((curLesson = EduKitLesson::LoadFromFile(filePath))) 
				AddLesson(curLesson);
		}
	}
}

void EduKitLessonLoaderWindow::AddLesson(EduKitLesson* lesson)
{
	int id = fListLessons->GetEntries();
	fListLessonBox->AddEntry(lesson->GetTitle(), id);

	fListLessons->Add(lesson);
}

void EduKitLessonLoaderWindow::ClearLessons()
{
	fListLessonBox->RemoveAll();
	fListLessons->Clear();
}

void EduKitLessonLoaderWindow::OnLessonSelected(Int_t lessonId)
{
	EduKitLesson* selectedLesson = (EduKitLesson*)fListLessons->At(lessonId);
	if(selectedLesson){
		fTextViewLessonInfo->SetText( selectedLesson->GetDescription() );
		fTextViewLessonInfo->SetWrapLength(fTextViewLessonInfo->GetWidth());
	}

	Layout();
}

void EduKitLessonLoaderWindow::OnAccepted()
{
	Accepted(0);
}

void EduKitLessonLoaderWindow::Accepted(EduKitLesson*)
{
	int selectedId = fListLessonBox->GetSelected();
	EduKitLesson* selectedLesson = (EduKitLesson*)fListLessons->At(selectedId);
	
	UnmapWindow();

	Long_t args[1];
	args[0] = (Long_t)selectedLesson;
	Emit("Accepted(EduKitLesson*)", args);


}
