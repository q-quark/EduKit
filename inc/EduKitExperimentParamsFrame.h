/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitExperimentParamsFrame.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#ifndef _EDUKIT_EXPERIMENT_PARAMSFRAME_H_
#define _EDUKIT_EXPERIMENT_PARAMSFRAME_H_ 

#include <TGFrame.h>

class EduKitExperimentParamsFrame : public TGGroupFrame
{
public:
	EduKitExperimentParamsFrame(const TGWindow* p);
	virtual ~EduKitExperimentParamsFrame();

	// Reimplement these slots in derived classes
	virtual void onAccepted();
	virtual void onCanceled();
	virtual void onDefaulted();	

public:
	ClassDef(EduKitExperimentParamsFrame,0)
};

#endif