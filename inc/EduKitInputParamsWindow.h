/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitInputParamsWindow.h) is part of EduKit.                              
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
 
#ifndef _EDUKIT_INPUTPARAMS_WINDOW_H_
#define _EDUKIT_INPUTPARAMS_WINDOW_H_

#include <TGFrame.h>

class TGTextButton;
class TGLabel;
class TGHorizontalFrame;

class EduKitExperimentParamsFrame;

class EduKitInputParamsWindow : public TGTransientFrame
{
public:
	EduKitInputParamsWindow(const TGWindow* p, const TGWindow* main=0, EduKitExperimentParamsFrame* paramsFrame=0, UInt_t width=250, UInt_t height=200);
	~EduKitInputParamsWindow();

	EduKitExperimentParamsFrame* GetParamsFrame() const;

	void SetParamsFrame(EduKitExperimentParamsFrame* paramsFrame);

	// call these slots to show/hide this dialog window
	void MapWindow(); // show
	void UnmapWindow(); // hide

	inline void Show(){ MapWindow(); }
	inline void Hide(){ UnmapWindow(); }

	void onAccepted();
	void onCanceled();
	void onDefaulted();

	void Accepted(); // *SIGNAL*
	void Canceled(); // *SIGNAL*

protected:
	void Setup(EduKitExperimentParamsFrame* settingsFrame);

	EduKitExperimentParamsFrame* fParamsFrame;
	TGLabel* fLabelInfo;

	TGHorizontalFrame* fLayoutButtonFrame;
	TGTextButton* fButtonOk;
	TGTextButton* fButtonCancel;
	TGTextButton* fButtonDefault;

public:
	ClassDef(EduKitInputParamsWindow,0)
	
};

#endif