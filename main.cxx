/*****************************************************************************************
 *                                                                                       
 *  This file (main.cxx) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#include <TApplication.h>
#include <TRint.h>

#include <TInterpreter.h>
#include <TSystem.h>
#include <TString.h>

#include "EduKitMain.h"

int main(int argc, char **argv)
{
	//TApplication app( "The Nuclear EduKit", &argc, argv );
	TRint *app = new TRint( "The Nuclear EduKit", &argc, argv );
	app->SetPrompt("edukit [%d] > ");

	TString bIncPath( Form("%s/include", gSystem->Getenv("EDUKIT_PATH")) );

	gSystem->AddIncludePath( Form(" -I%s ", bIncPath.Data()) );
	gInterpreter->AddIncludePath(bIncPath.Data());

	EduKitMain::Instance();

	app->Connect("EduKitMainWindow", "CloseWindow()", "TApplication", app, "Terminate()");
	app->Run();

	return 0;
}