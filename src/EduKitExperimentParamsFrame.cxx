/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitExperimentParamsFrame.cxx) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
 

#include "EduKitExperimentParamsFrame.h"

ClassImp(EduKitExperimentParamsFrame)
EduKitExperimentParamsFrame::EduKitExperimentParamsFrame(const TGWindow* p)
	: TGGroupFrame(p,  "Experiment:", kVerticalFrame)
{

}

EduKitExperimentParamsFrame::~EduKitExperimentParamsFrame()
{

}

// Reimplement these slots in derived classes
void EduKitExperimentParamsFrame::onAccepted(){ AbstractMethod("onAccepted"); }
void EduKitExperimentParamsFrame::onCanceled(){ AbstractMethod("onCanceled"); }
void EduKitExperimentParamsFrame::onDefaulted(){ AbstractMethod("onDefaulted"); }	