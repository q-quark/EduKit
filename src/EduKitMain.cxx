/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitMain.cxx) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#include <TInterpreter.h>
#include <TSystem.h>

#include <TObject.h>
#include <TCanvas.h>
#include <TString.h>
#include <TRootEmbeddedCanvas.h>

#include "EduKitNucleusInfoDB.h"
#include "EduKitLesson.h"
#include "EduKitSimEngine.h"
#include "EduKitPlotManager.h"
#include "EduKitInfoWidget.h"
#include "EduKitMainWindow.h"
#include "EduKitMain.h"

EduKitMain* gEduKit=0;

ClassImp(EduKitMain)

EduKitMain* EduKitMain::Instance()
{
	if(gEduKit == 0){
		gEduKit = new EduKitMain();
	}

	return gEduKit;
}

EduKitMain::EduKitMain()
	: fMainWindow(0)
{
	if(gEduKit != 0 ) throw("There can be only one EduKit");

	gEduKit = this;

	fMainWindow = new EduKitMainWindow(gClient->GetRoot(), 800, 600);
	fMainWindow->SetWindowName("The Nuclear EduKit");
}

EduKitMain::~EduKitMain()
{
 // !!! To be done !!!
}

TCanvas* EduKitMain::GetCanvas() const
{
	TCanvas* mainCanvas = fMainWindow->fAnimationWidget->GetCanvas();
	gPad = mainCanvas->cd(0);

	return mainCanvas;
}

EduKitExperiment* EduKitMain::GetExperiment() const
{
	return fMainWindow->fSimEngine->GetExperiment();
}

EduKitLesson* EduKitMain::GetCurrentLesson() const
{
	return fMainWindow->fCurrentLesson;
}

const char* EduKitMain::GetResourcePath() const
{
	return Form("%s/res", gSystem->Getenv("EDUKIT_PATH") );
}

EduKitNucleusInfoDB* EduKitMain::NucleaiDB() const
{
	return fMainWindow->fNucleaiDB;
}

// ------------ Plots Methods ------------ 
TObject* EduKitMain::NewPlot(const char* className, const char* name, const char* title)
{ 
	return fMainWindow->fPlotManager->NewPlot(className, name, title); 
}

void EduKitMain::RemovePlot(const char* name)
{
	fMainWindow->fPlotManager->RemovePlot(name);
}

TObject* EduKitMain::GetPlot(const char* name) const
{
	return fMainWindow->fPlotManager->GetPlot(name); 
}

void EduKitMain::ShowPlot(const char* name)
{
	fMainWindow->fPlotManager->ShowPlot(name);
}

void EduKitMain::HidePlot(const char* name)
{
	fMainWindow->fPlotManager->HidePlot(name);
}

void EduKitMain::ClearPlots()
{
	fMainWindow->fPlotManager->Clear();
}

void EduKitMain::RegisterLesson(const char* name, EduKitLesson* lesson)
{
	//fMainWindow->LoadLesson(lesson);
}

// need some memory management here. 
// We have mem leak because curlesson is never released.
void EduKitMain::LoadLesson(const char* name)
{
	const char* filePath =  Form("%s/lessons/%s/lesson.conf", gSystem->Getenv("EDUKIT_PATH") , name);

	printf("Loading Lesson From File: %s\n", filePath);
	EduKitLesson* curLesson = EduKitLesson::LoadFromFile(filePath);
	fMainWindow->LoadLesson(curLesson);
}

void EduKitMain::SetInfoText(const char* infoText)
{
	fMainWindow->fInfoWidget->SetText(infoText);
}

void EduKitMain::UpdateDraw()
{
	TCanvas* mainCanvas = fMainWindow->fAnimationWidget->GetCanvas();
	TList* listSubPads = mainCanvas->GetListOfPrimitives();

	if(listSubPads){
		// update subpads
		for(int i=0; i<listSubPads->GetEntries(); i++)
		{
			TObject* p = (TPad*)listSubPads->At(i);
			if(p->InheritsFrom(TVirtualPad::Class())){
				((TPad*)p)->Modified();
				((TPad*)p)->Update();
			}
		}
	}

	mainCanvas->Modified();
	mainCanvas->Update();
	fMainWindow->fPlotManager->UpdateDraw();
}
