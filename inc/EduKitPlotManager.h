/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitPlotManager.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
 
#ifndef _EDUKIT_PLOTMANAGER_H_
#define _EDUKIT_PLOTMANAGER_H_

#include <map>
#include <string>

#include <Rtypes.h>

class TObject;
class TGPopupMenu;
class TGWindow;

class EduKitPlotWindow;

typedef std::map<std::string,TObject*> EduKitObjectsMap;
typedef std::map<TObject*,Int_t> EduKitObjectsIdsMap;

class EduKitPlotManager
{
public:
	EduKitPlotManager(TGPopupMenu* menu); // manages this menu
	~EduKitPlotManager();

	TObject* NewPlot(const char* className, const char* name, const char* title);
	void RemovePlot(const char* name);

	TObject* GetPlot(const char* name) const;

	void ShowPlot(const char* name);
	void HidePlot(const char* name);
	void ShowAllPlots();
	void HideAllPlots();

	void Clear();

	void UpdateDraw();
protected:
	EduKitPlotWindow* GetPlotWindow(const char* name) const;

	TGPopupMenu* fMenu; // parent window to which all plots belong to
	EduKitObjectsMap fPlotsMap; // a map with all plots (maps Plot Name => Its PlotWindow )
	EduKitObjectsIdsMap fPlotsIdsMap; // a map with all plots (maps Plot Name => Its PlotWindow )
	Int_t fCurPlotIdsMax; // current attained maximum ID used by the Plots menu
private:
	EduKitPlotManager(const EduKitPlotManager& ); // not implemented
	EduKitPlotManager& operator=(const EduKitPlotManager&); // not implemented

public:
	ClassDef(EduKitPlotManager,0)
};
 #endif