/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitLessonLoaderWindow.h) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
 
#ifndef _EDUKIT_LESSON_LOADER_WINDOW_H_
#define _EDUKIT_LESSON_LOADER_WINDOW_H_ 

#include <TGFrame.h>

class TGListBox;
class TGTextButton;
class TGTextView;
class TList;
class TGLabel;

class EduKitLesson;


class EduKitLessonLoaderWindow : public TGTransientFrame
{
public:
	EduKitLessonLoaderWindow(const TGWindow* p, const TGWindow* main = 0, UInt_t width=250, UInt_t height=200);
	~EduKitLessonLoaderWindow();

	void PopulateLessonList();

	void MapWindow();
	void UnmapWindow();

	// slots
	void OnAccepted();
	void OnLessonSelected(Int_t lessonId);

	// signals
	void Accepted(EduKitLesson*); //*SIGNAL*
protected:
	void AddLesson(EduKitLesson* lesson);
	void ClearLessons();

	TGListBox* fListLessonBox; // diplay all available lessons
	TGTextButton* fButtonLoadLesson; // load current selected lesson
	TGTextButton* fButtonCancelLoadLesson; // cancel/close current selected lesson
	TGLabel* fTextViewLessonInfo; // textview for diplaying lesson information

	TList* fListLessons; // A list of loaded EduKitLessons

public:
	ClassDef(EduKitLessonLoaderWindow,0)
};

#endif