void create_lesson_conf()
{
	EduKitLesson* l = new EduKitLesson("beta");

	l->SetTitle("Beta decay");
	l->SetDescription("Beta decay represents the spontaneous process of radioactive decay of an unstable nuclei with emission of an electron and antineutrino in beta minus decay, a positron and a neutrino in beta plus and with an electron capture for the last possible situation.");
	l->SetExperimentClass("BetaExperiment"); // class/file that implements EduKitExperiment
	l->SetExperimentFile("Beta.cxx"); // class/file that implements EduKitExperiment
	l->SetManual("index.html");
	l->WriteFile("lesson.conf");
}