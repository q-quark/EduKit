#ifndef EXAMPLE_CLASSES_H__
#define EXAMPLE_CLASSES_H__ 

#include <TMath.h>

#include "EduKitExperiment.h"
#include "EduKitExperimentParamsFrame.h"

class TF1;
class TMarker;
class TText;
class TArrow;

class BetaDecayExperimentParamsFrame;
class EduKitNucleus;

class BetaExperiment : public EduKitExperiment
{
public:
	// MUST have at least one default constructor
	BetaExperiment();
	~BetaExperiment();

	void Init();
	void NextStep(Int_t stepNumber);
	void ClearAll();


    // return nuclei mother properties
    Int_t GetA() const;
    Int_t GetZ() const;

    void SetParams(Int_t A, Int_t Z);
    EduKitExperimentParamsFrame* GetParamsFrame();

	void SetDefaults();

	// Other methods

protected:
    int fMotherA;
    int fMotherZ;
    int fDaughterA;
    int fDaughterZ;

	TF1* fElectronEFormula;
	TF1* fElectronPFormula;

	EduKitNucleus *fMother;
    EduKitNucleus *fDaughter;
    EduKitNucleus *fElectron;
    EduKitNucleus *fNeutrino;

    TMarker* fMotherMarker;
    TMarker* fDaughterMarker;
    TMarker* fBetaMarker;
    TMarker* fNeutrinoMarker;

    TText* fMotherText;
    TText* fDaughterText;
    TText* fBetaText;
    TText* fNeutrinoText;

    TArrow* fDaughterArrow;
    TArrow* fBetaArrow;
    TArrow* fNeutrinoArrow;

    // GUI Window to setup the simulation variables
    BetaDecayExperimentParamsFrame* fParamsFrame;
public:
	ClassDef(BetaExperiment, 0)
};


//______________________________________________________________________________

class TGTextEntry;

class BetaDecayExperimentParamsFrame : public EduKitExperimentParamsFrame
{
public:
    BetaDecayExperimentParamsFrame();
    BetaDecayExperimentParamsFrame(BetaExperiment* exp);
    ~BetaDecayExperimentParamsFrame(){}

    void onAccepted();
    void onCanceled();
    void onDefaulted(); 

protected:
    void LoadData(); // loads all data and set it to GUI objects

    BetaExperiment* fExp; // parent experiment
    TGTextEntry* fEntryA;
    TGTextEntry* fEntryZ;
public:
    ClassDef(BetaDecayExperimentParamsFrame, 0)
};
#endif