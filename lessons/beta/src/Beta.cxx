#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstdlib>
#include <iomanip>

#include <TArrow.h>
#include <TMarker.h>
#include <TText.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TMath.h>
#include <TString.h>
#include <TRandom.h>
#include <TF1.h>

#include <TH1.h>

#include <TGLabel.h>
#include <TGDoubleSlider.h>
#include <TGTextEntry.h>
#include <TGLayout.h>

#include "EduKitNucleus.h"
#include "EduKitNucleusInfoDB.h"
#include "EduKitNucleusInfo.h"

#include "EduKitMain.h"
#include "EduKitLesson.h"
#include "Beta.h"

using namespace std;

class BetaDecayExperimentParamsFrame;

 
inline void printDBEntry(EduKitNucleusInfo *dbEntry)
{
     //cout.precision(5);
     cout << "Entry (" <<dbEntry->Symbol.c_str() << ") A: " << dbEntry->A << " Z: " <<  dbEntry->Z << " Mass:"<< dbEntry->Mass <<" MassExcess:" << dbEntry->MassExcess << endl;
}


Double_t ElectronEnergyFermi(Double_t* x, Double_t* param)
{
    return param[0]*TMath::Sqrt(x[0]*x[0] + 2.0*x[0]*param[2])*(param[1] - x[0])*(param[1] - x[0])*(x[0]+param[2]);
}

Double_t ElectronPFermi(Double_t* x, Double_t* param)
{
    return param[0]* x[0]*x[0] * TMath::Power(param[1] - TMath::Sqrt(x[0]*x[0] + param[2]*param[2]) + param[2], 2);
}

void RotateMarker(TMarker* marker, Double_t angle)
{
    Double_t x = marker->GetX();
    Double_t y = marker->GetY();

    Double_t xtemp = x;

    x = x*TMath::Cos(angle) - y * TMath::Sin(angle);
    y = xtemp*TMath::Sin(angle) + y * TMath::Cos(angle);

    marker->SetX(x);
    marker->SetY(y);
}

void RotateMarkerText(TText* text, Double_t angle)
{
    Double_t x = text->GetX();
    Double_t y = text->GetY();

    Double_t xtemp = x;

    x = x*TMath::Cos(angle) - y * TMath::Sin(angle);
    y = xtemp*TMath::Sin(angle) + y * TMath::Cos(angle);

    text->SetX(x);
    text->SetY(y);
}


Double_t GradToRad(Double_t grade)
{
    Double_t rad = grade*TMath::Pi()/180.0;
    return rad;
}

// Experiment implementation
ClassImp(BetaExperiment);
ClassImp(BetaDecayExperimentParamsFrame);
BetaExperiment::BetaExperiment()
	: EduKitExperiment("betaDecay", "Dezintegrarea Beta"),
      fMotherA(135),
      fMotherZ(55),
      fMother(0),
      fDaughter(0),
      fElectron(0),
      fNeutrino(0),
      fElectronEFormula(0),
      fElectronPFormula(0),
      fMotherMarker(0),
      fDaughterMarker(0),
      fBetaMarker(0),
      fNeutrinoMarker(0),
      fMotherText(0),
      fDaughterText(0),
      fBetaText(0),
      fNeutrinoText(0),
      fDaughterArrow(0),
      fBetaArrow(0),
      fNeutrinoArrow(0),
      fParamsFrame(0)
{
    SetDefaults();

    fParamsFrame = new BetaDecayExperimentParamsFrame(this);
}

BetaExperiment::~BetaExperiment()
{
  
}

void BetaExperiment::SetParams(Int_t A, Int_t Z)
{
    fMotherA = A;
    fMotherZ = Z;
}

Int_t BetaExperiment::GetA() const
{
    return fMotherA;
}

Int_t BetaExperiment::GetZ() const
{
    return fMotherZ;
}

EduKitExperimentParamsFrame* BetaExperiment::GetParamsFrame()
{
    return fParamsFrame;
}

void BetaExperiment::Init()
{
    // parametrii intrare
    fDaughterA = fMotherA;

    EduKitNucleusInfoDB* DB = gEduKit->NucleaiDB();


    fMother   = new EduKitNucleus(DB->GetEntry(fMotherA, fMotherZ));
    fNeutrino = new EduKitNucleus(DB->GetEntry(0,0));

    Double_t motherMass = fMother->GetInfo()->Mass;

    if(motherMass<1e-3) {
        printf("No Particle with (A:%d, Z:%d) was found  in the database! Exiting Init...\n", fMotherA, fMotherZ );
        return;
    }

     printf("Found Particle (A:%d, Z:%d) with Mass:%g MeV\n", fMotherA, fMotherZ, motherMass );

    fDaughterZ=0;
    bool IsBetaMinus=1;

    if(IsBetaMinus)
    {
        fDaughterZ = fMotherZ + 1;
        fElectron = new EduKitNucleus(DB->GetEntry(0, -1));
    }
    else
    {
        fDaughterZ = fMotherZ - 1;
        fElectron = new EduKitNucleus(DB->GetEntry(0, 1));
    }
    
    fDaughter = new EduKitNucleus(DB->GetEntry(fDaughterA, fDaughterZ));

    Double_t daughterMass = fDaughter->GetInfo()->Mass;

    //cout.precision(5);
    cout << "\n============= MotherMass:"<< motherMass << " DaughterMass:"<< daughterMass << " ===== BetaMass:" << fElectron->GetInfo()->Mass << endl;


    // Animation

    // make active the current canvas
    TCanvas* pad = gEduKit->GetCanvas();
    
    if(!pad) return;
    pad->Range(-400,-600, 400,600); 

    fMotherMarker = new TMarker;
    fMotherMarker->SetMarkerStyle(8);
    fMotherMarker->SetMarkerSize(16);
    fMotherMarker->SetMarkerColor(7);

    fMotherMarker->SetX(0);
    fMotherMarker->SetY(0);

    fMotherMarker->Draw();

    fMotherText = new TText(0, 0, fMother->GetInfo()->Symbol.c_str());
    fMotherText->SetTextAlign(22);
    fMotherText->SetTextColor(1);
    fMotherText->SetTextFont(43);
    fMotherText->SetTextSize(50);
    fMotherText->SetTextAngle(0);
    fMotherText->Draw();

    fDaughterMarker = new TMarker;
    fDaughterMarker->SetMarkerStyle(8);
    fDaughterMarker->SetMarkerSize(12);
    fDaughterMarker->SetMarkerColor(2);
    fDaughterMarker->SetX(100);
    fDaughterMarker->SetY(300);

    fDaughterMarker->Draw();

    fDaughterArrow = new TArrow(0,0, 100, 300);
    fDaughterArrow->Draw();

    fDaughterText = new TText(100, 300, fDaughter->GetInfo()->Symbol.c_str());
    fDaughterText->SetTextAlign(22);
    fDaughterText->SetTextColor(1);
    fDaughterText->SetTextFont(43);
    fDaughterText->SetTextSize(40);
    fDaughterText->SetTextAngle(0);
    fDaughterText->Draw();
    
    fBetaMarker = new TMarker;
    fBetaMarker->SetMarkerStyle(8);
    fBetaMarker->SetMarkerSize(4);
    fBetaMarker->SetMarkerColor(3);

    fBetaMarker->SetX(300);
    fBetaMarker->SetY(400);

    fBetaMarker->Draw();

    fBetaText = new TText(300, 400, "e");
    fBetaText->SetTextAlign(22);
    fBetaText->SetTextColor(1);
    fBetaText->SetTextFont(43);
    fBetaText->SetTextSize(20);
    fBetaText->SetTextAngle(0);
    fBetaText->Draw();

    fBetaArrow = new TArrow(0,0, 300, 400);
    fBetaArrow->Draw();

    fNeutrinoMarker = new TMarker;
    fNeutrinoMarker->SetMarkerStyle(8);
    fNeutrinoMarker->SetMarkerSize(1);
    fNeutrinoMarker->SetMarkerColor(6);

    fNeutrinoMarker->SetX(300);
    fNeutrinoMarker->SetY(200);

    fNeutrinoMarker->Draw();

    fNeutrinoArrow = new TArrow(0,0, 300, 200);
    fNeutrinoArrow->Draw();

    // fNeutrinoText = new TText(300, 200, "#nu");
    // fNeutrinoText->SetTextAlign(22);
    // fNeutrinoText->SetTextColor(1);
    // fNeutrinoText->SetTextFont(43);
    // fNeutrinoText->SetTextSize(50);
    // fNeutrinoText->SetTextAngle(0);
    // fNeutrinoText->Draw();

    Double_t Qmax = fMother->GetInfo()->Mass - fDaughter->GetInfo()->Mass;
    cout << "\n====== QMax:" << Qmax << " =========\n" << endl;

    if(Qmax<=1e-6) 
    {
        printf("Reaction is not beta decay. Exiting...\n");

        ClearAll();
    }

      // Histograms
    TH1F* eEnergy = (TH1F*)gEduKit->NewPlot("TH1F", "eEnergy", "Histogram of electron Energy");
    eEnergy->SetBins( 50 ,0, Qmax);
    eEnergy->SetMinimum(1);
    eEnergy->SetMaximum((fSimStepEnd - fSimStepStart)/fSimStepStep/2.0);
    eEnergy->SetStats(0);
    eEnergy->SetLineColor(4);
    eEnergy->GetXaxis()->SetTitle("electron Energy [MeV]");
    eEnergy->GetYaxis()->SetTitle("Intensity");
    eEnergy->Draw(); // we need to do this initially in order to add it to the current Plot Canvas

    gEduKit->ShowPlot("eEnergy");


    // TH1F* daughterEnergy = (TH1F*)gEduKit->NewPlot("TH1F", "daughterEnergy", "Histogram of Daughter Energy");
    
    // daughterEnergy->SetBins(10,20,100000);
    // daughterEnergy->SetMinimum(1);
    // daughterEnergy->SetMaximum(50);
    // daughterEnergy->SetStats(0);
    // daughterEnergy->SetLineColor(4);
    // daughterEnergy->GetXaxis()->SetTitle("Daughter Energy [MeV]");
    // daughterEnergy->GetYaxis()->SetTitle("Frequency");
    // daughterEnergy->Draw(); // we need to do this initially in order to add it to the current Plot Canvas

    // gEduKit->ShowPlot("daughterEnergy");


    fElectronEFormula = new TF1("eEnergyFormula", ElectronEnergyFermi, 0.0, Qmax, 3);
    fElectronEFormula->SetParNames("normalizationConstant", "Qmax", "electron mass");
    fElectronEFormula->SetParameter(0, 1.0);
    fElectronEFormula->SetParameter(1, Qmax);
    fElectronEFormula->SetParameter(2, 0.511); // electron mass

    Double_t Pmax = TMath::Sqrt(Qmax*Qmax + 2* fElectron->GetInfo()->Mass * Qmax);

    fElectronPFormula = new TF1("ePFormula", ElectronPFermi, 0.0, Pmax, 3);
    fElectronPFormula->SetParNames("normalizationConstant", "Pmax", "electron mass");
    fElectronPFormula->SetParameter(0, 1.0);
    fElectronPFormula->SetParameter(1, Pmax);
    fElectronPFormula->SetParameter(2, 0.511); // electron mass
}

void BetaExperiment::NextStep(Int_t /*stepNumber*/)
{
    //if(fElectron==0 || fMother==0 || fNeutrino==0 || fDaughter==0) return;

    fElectron->SetKEnergy( fElectronEFormula->GetRandom() );
    fElectron->SetMomentum( fElectronPFormula->GetRandom() );

    // Grafice si animatie

    TCanvas* mainCanvas = gEduKit->GetCanvas();

    Double_t alpha;

    Double_t randTeta = gRandom->Uniform(0, 90.0);
    Double_t teta = GradToRad(randTeta);

    Double_t randPhi = gRandom->Uniform(0, 90.0);
    Double_t phi = GradToRad(randPhi);

    alpha = phi + teta;

     cout << "randPhi" << randPhi << " randTeta " << randTeta<< endl;
    cout << "randAlpha" << randPhi + randTeta << endl;

    Double_t Q = fMother->GetInfo()->Mass - fDaughter->GetInfo()->Mass - fElectron->GetKEnergy();
    Double_t B = 2*fElectron->GetMomentum()*TMath::Cos(alpha) + 2*fDaughter->GetInfo()->Mass;
    Double_t C = TMath::Power(fElectron->GetMomentum(), 2.0) - 2*fDaughter->GetInfo()->Mass *Q;

    Double_t delta = B*B - 4.0*C;

    cout << " delta = " << delta << endl;

    // Neutrino
    fNeutrino->SetMomentum( (-B + TMath::Sqrt(delta))/2.0 );
    fNeutrino->SetKEnergy(fNeutrino->GetMomentum());

    Double_t pDelta = Q - fNeutrino->GetMomentum();

     cout << " pDelta = " << pDelta << endl;
    
    // Daughter
    fDaughter->SetMomentum( TMath::Sqrt(2.0*fDaughter->GetInfo()->Mass*pDelta)  );
    fDaughter->SetKEnergy(TMath::Power(fDaughter->GetMomentum(),2.0) / (2.0* fDaughter->GetInfo()->Mass) );

    // --------- Drawing
    Double_t scale = 2 * TMath::Max(fElectron->GetMomentum(), fNeutrino->GetMomentum());

    mainCanvas->Range(-scale,-scale, scale, scale);

    fDaughterMarker->SetX(-fDaughter->GetMomentum());
    fDaughterMarker->SetY(0);
    fDaughterText->SetX(fDaughterMarker->GetX());
    fDaughterText->SetY(0);

    fBetaMarker->SetX(fElectron->GetMomentum()*TMath::Cos(teta));
    fBetaMarker->SetY(fElectron->GetMomentum()*TMath::Sin(teta));
    fBetaText->SetX(fBetaMarker->GetX());
    fBetaText->SetY(fBetaMarker->GetY());

    fNeutrinoMarker->SetX(fNeutrino->GetMomentum()*TMath::Cos(phi));
    fNeutrinoMarker->SetY(-fNeutrino->GetMomentum()*TMath::Sin(phi));
    // fNeutrinoText->SetX(fNeutrinoMarker->GetX());
    // fNeutrinoText->SetY(fNeutrinoMarker->GetY());

    Double_t rotAngle =  GradToRad(gRandom->Uniform(0, 360));
   
   cout << "rotAngle" << rotAngle << endl;

    RotateMarker(fDaughterMarker, rotAngle);
    RotateMarker(fBetaMarker, rotAngle);
    RotateMarker(fNeutrinoMarker, rotAngle);

    RotateMarkerText(fDaughterText, rotAngle);
    RotateMarkerText(fBetaText, rotAngle);
    // RotateMarkerText(fNeutrinoText, rotAngle);

    fDaughterArrow->SetX2(fDaughterMarker->GetX());
    fDaughterArrow->SetY2(fDaughterMarker->GetY());

    fBetaArrow->SetX2(fBetaMarker->GetX());
    fBetaArrow->SetY2(fBetaMarker->GetY());

    fNeutrinoArrow->SetX2(fNeutrinoMarker->GetX());
    fNeutrinoArrow->SetY2(fNeutrinoMarker->GetY());

    TH1F* eEnergy = (TH1F*)gEduKit->GetPlot("eEnergy"); 
   // TH1F* daughterEnergy = (TH1F*)gEduKit->GetPlot("daughterEnergy"); 
    eEnergy->Fill(fElectron->GetKEnergy());
   // daughterEnergy->Fill(fDaughter->GetEnergy());

    cout << "Energia electron este: " << fElectron->GetKEnergy() << endl;
    cout << "Energia fiica este: " << fDaughter->GetKEnergy() << "\t" << fDaughter->GetMomentum() << endl;

    gEduKit->UpdateDraw();
}

void BetaExperiment::ClearAll()
{
    delete fMother; fMother = 0;
    delete fDaughter; fDaughter = 0;
    delete fElectron; fElectron = 0;
    delete fNeutrino; fNeutrino = 0;

    delete fMotherMarker; fMotherMarker=0;
    delete fMotherText; fMotherText=0;
    delete fDaughterMarker; fDaughterMarker=0;
    delete fDaughterText; fDaughterText=0;
    delete fBetaMarker; fBetaMarker=0;
    delete fBetaText; fBetaText=0;
    delete fNeutrinoMarker; fNeutrinoMarker=0;

    delete fDaughterArrow; fDaughterArrow=0;
    delete fBetaArrow; fBetaArrow=0;
    delete fNeutrinoArrow; fNeutrinoArrow=0;
  //  delete fNeutrinoText; fNeutrinoText=0;


    gEduKit->ClearPlots();
}

void BetaExperiment::SetDefaults()
{
    fMotherA = 135;
    fMotherZ = 55;

    fSimStepEnd = 1000;
    fSimStepStep = 1;
    fSimStepStart = 0;
    fSimTimeout = 200;

}


/********************************************************************************
 *
 *        GUI  for the Experiments Settings
 *
 *
 *******************************************************************************/

//______________________________________________________________________________
BetaDecayExperimentParamsFrame::BetaDecayExperimentParamsFrame()
    : EduKitExperimentParamsFrame( gClient->GetRoot()),
    fExp(0),
    fEntryA(0),
    fEntryZ(0)
{
    SetCleanup(kDeepCleanup);

    SetLayoutManager(new TGMatrixLayout(this, 0, 2, 10));

    // Row Half-Life
    TGLabel* labelAtomA =  new TGLabel(this, " Atomic mass number (A):");
    fEntryA = new TGTextEntry(this, "135");
    AddFrame(labelAtomA);
    AddFrame(fEntryA);

    TGLabel* labelAtomNumber =  new TGLabel(this, "Atomic number (Z):");
    fEntryZ = new TGTextEntry(this, "55");
    AddFrame(labelAtomNumber);
    AddFrame(fEntryZ);

    onDefaulted();

    //fEntrySimEventsNumber->Connect("TextChanged(const char*)",  "BetaDecayExperimentParamsFrame", this, "UpdateTimeStepLabel()");

    Resize();
}

BetaDecayExperimentParamsFrame::BetaDecayExperimentParamsFrame(BetaExperiment* exp)
    : EduKitExperimentParamsFrame( gClient->GetRoot()),
    fExp(exp),
    fEntryA(0),
    fEntryZ(0)
{
    SetCleanup(kDeepCleanup);

    SetLayoutManager(new TGMatrixLayout(this, 0, 2, 10));

    // Row Half-Life
    TGLabel* labelAtomA =  new TGLabel(this, "Atomic mass number (A):");
    fEntryA = new TGTextEntry(this, "135");
    AddFrame(labelAtomA);
    AddFrame(fEntryA);

    TGLabel* labelAtomNumber =  new TGLabel(this, "Atomic number (Z):");
    fEntryZ = new TGTextEntry(this, "55");
    AddFrame(labelAtomNumber);
    AddFrame(fEntryZ);

    onDefaulted();

    Resize(400, 200);
}

void BetaDecayExperimentParamsFrame::onAccepted()
{
    TString nucleaiAStr(fEntryA->GetText());
    TString nucleaiZStr(fEntryZ->GetText());

    if(fExp){
        // set basic simulation data
        fExp->SetParams(nucleaiAStr.Atoi(), nucleaiZStr.Atoi());
    }
}

void BetaDecayExperimentParamsFrame::onCanceled()
{
    // do nothing
}

void BetaDecayExperimentParamsFrame::onDefaulted()
{
    if(!fExp) return;

    fExp->SetDefaults();

    LoadData();
}

void BetaDecayExperimentParamsFrame::LoadData()
{
    if(!fExp) return;

    fEntryA->SetText( Form("%d", fExp->GetA()) );
    fEntryZ->SetText( Form("%d", fExp->GetZ()) );

    Layout();
}
