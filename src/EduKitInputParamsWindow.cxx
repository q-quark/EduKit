/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitInputParamsWindow.cxx) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#include <TTimer.h>
#include <TGButton.h>
#include <TGLabel.h>


#include "EduKitExperimentParamsFrame.h"
#include "EduKitInputParamsWindow.h"

ClassImp(EduKitInputParamsWindow)
EduKitInputParamsWindow::EduKitInputParamsWindow(const TGWindow* p, const TGWindow* main, EduKitExperimentParamsFrame* paramsFrame, UInt_t width, UInt_t height)
	: TGTransientFrame(p, main, width, height,  kTransientFrame),
	fParamsFrame(0),
	fLabelInfo(0),
	fLayoutButtonFrame(0),
	fButtonOk(0),
	fButtonCancel(0),
	fButtonDefault(0)
{
	SetCleanup(kDeepCleanup);

	Setup(paramsFrame);

	SetWindowName("Simulation Settings");

	SetMWMHints(kMWMDecorTitle | kMWMDecorBorder | kMWMDecorResizeH, 
			kMWMFuncResize | kMWMFuncMove,
			kMWMInputModeless);

	Connect("CloseWindow()", "EduKitInputParamsWindow", this, "Hide()");
}

EduKitInputParamsWindow::~EduKitInputParamsWindow()
{

}

EduKitExperimentParamsFrame* EduKitInputParamsWindow::GetParamsFrame() const
{
	return fParamsFrame;
}

void EduKitInputParamsWindow::SetParamsFrame(EduKitExperimentParamsFrame* paramsFrame)
{
	// if(paramsFrame==0) {
	// 	if(fParamsFrame)
	// 	{
	// 	   TGFrameElement *el = FindFrameElement(fParamsFrame);

	// 	   if (el) {
	// 	      fList->Remove(el);
	// 	      if (el->fLayout) el->fLayout->RemoveReference();
	// 	      delete el;
	// 	   }
	// 	}

	// 	fParamsFrame = 0;
	// }

	Setup(paramsFrame);
}

void EduKitInputParamsWindow::Setup(EduKitExperimentParamsFrame* settingsFrame)
{
	//Cleanup();

	if(settingsFrame==0){
		fLabelInfo = new TGLabel(this, "No Properties to show");
		AddFrame(fLabelInfo, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY) );
	}
	else {
		fParamsFrame = settingsFrame;
		fParamsFrame->ReparentWindow(this);
		AddFrame(fParamsFrame, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY) );
	}

	fLayoutButtonFrame = new TGHorizontalFrame(this);

	fButtonOk = new TGTextButton(fLayoutButtonFrame, " Ok ");
	fButtonCancel = new TGTextButton(fLayoutButtonFrame, "Cancel");
	fButtonDefault = new TGTextButton(fLayoutButtonFrame, "Defaults");

	fLayoutButtonFrame->AddFrame(fButtonDefault, new TGLayoutHints(kLHintsExpandX | kLHintsLeft) );
	fLayoutButtonFrame->AddFrame(fButtonCancel, new TGLayoutHints(kLHintsExpandX | kLHintsRight) );
	fLayoutButtonFrame->AddFrame(fButtonOk, new TGLayoutHints(kLHintsExpandX | kLHintsRight) );

	fButtonOk->Connect("Released()", "EduKitInputParamsWindow", this, "onAccepted()");
	fButtonCancel->Connect("Released()", "EduKitInputParamsWindow", this, "onCanceled()");
	fButtonDefault->Connect("Released()", "EduKitInputParamsWindow", this, "onDefaulted()");
	
	AddFrame(fLayoutButtonFrame, new TGLayoutHints(kLHintsExpandX) );

	if(IsMapped()) Show();
}

void EduKitInputParamsWindow::onAccepted()
{
	// The same effect can be obtained by using a singleshot timer:
	Accepted();
	
	TTimer::SingleShot(0, "EduKitInputParamsWindow", this, "Hide()");
}

void EduKitInputParamsWindow::onCanceled()
{
	// The same effect can be obtained by using a singleshot timer:
	Canceled();

	TTimer::SingleShot(0, "EduKitInputParamsWindow", this, "Hide()");
}

void EduKitInputParamsWindow::onDefaulted()
{
	if(fParamsFrame) fParamsFrame->onDefaulted();
}

void EduKitInputParamsWindow::MapWindow()
{
	printf("EduKitInputParamsWindow::MapWindow()");

	MapSubwindows();
	TGTransientFrame::MapWindow();
	Layout();
	CenterOnParent();

	fClient->WaitFor(this);
}

void EduKitInputParamsWindow::UnmapWindow()
{
	DontCallClose();
	Layout();

	TGTransientFrame::UnmapWindow();

	fClient->ResetWaitFor(this);
}

void EduKitInputParamsWindow::Accepted()
{
	Emit("Accepted()");
}

void EduKitInputParamsWindow::Canceled()
{
	Emit("Canceled()");
}