/*****************************************************************************************
 *                                                                                       
 *  This file (DecaysExperimentParamsFrame.cxx) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/

#include "EduKitMain.h"

#include "TestClasses.h"

ClassImp(TestExperiment)

TestExperiment::TestExperiment():
	EduKitExperiment("test", "Acesta este titlul experimentului de test") //cheama automat constructorul clasei parinte
	{ 

	}//constructor default, must have!!!!!
TestExperiment::~TestExperiment()
	{

	}
void TestExperiment::Init()
{

}
void TestExperiment::NextStep(Int_t stepNumber)
{

}
void TestExperiment::ClearAll()
{

}
void TestExperiment::SetDefaults()
{

}
