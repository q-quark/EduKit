#ifndef EXAMPLE_CLASSES_H__
#define EXAMPLE_CLASSES_H__ 

#include "EduKitExperiment.h"

class TestExperiment : public EduKitExperiment
{
public:
	// MUST have at least one default constructor
	TestExperiment();
	~TestExperiment();

	void Init();
	void NextStep(Int_t stepNumber);
	void ClearAll();

	void SetDefaults();

	// Other methods

protected:
	// Here put your class variable members	

public:
	ClassDef(TestExperiment, 0)
};
#endif