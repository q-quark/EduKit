/*****************************************************************************************
 *                                                                                       
 *  This file (EduKitInfoWidget.cxx) is part of EduKit.                                          
 *                                                                                       
 *  EduKit is free software: you can redistribute it and/or modify                       
 *  it under the terms of the GNU General Public License as published by                 
 *  the Free Software Foundation, either version 3 of the License, org                   
 *  (at your option) any later version.                                                  
 *                                                                                       
 *  EduKit is distributed in the hope that it will be useful,                            
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                       
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                        
 *  GNU General Public License for more details.                                         
 *                                                                                       
 *  You should have received a copy of the GNU General Public License                    
 *  along with EduKit.  If not, see <http://www.gnu.org/licenses/>                       
 *                                                                                       
 *****************************************************************************************/
 
#include <TGTextView.h>

#include "EduKitInfoWidget.h"

ClassImp(EduKitInfoWidget)

EduKitInfoWidget::EduKitInfoWidget(const TGWindow* p)
	: TGVerticalFrame(p),
	fTextView(0)
{
	fTextView = new TGTextView(this);
	fTextView->SetMinHeight(150);
	fTextView->SetMaxHeight(200);

	AddFrame(fTextView, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY) );

	SetMinHeight(160);
	SetMaxHeight(250);

	SetHeight(200);

	MapSubwindows();
	MapWindow();
	Layout();

}

EduKitInfoWidget::~EduKitInfoWidget()
{

}

void EduKitInfoWidget::SetText(const char* fulltext)
{
	fTextView->LoadBuffer(fulltext);
}