#include <TROOT.h>

#include "EduKitUtils.h"

// thieft from TEveUtil.cxx (ROOT)

namespace
{
//______________________________________________________________________________
void ChompTailAndDir(TString& s, char c='.')
{
   // Remove last part of string 's', starting from the last
   // occurrence of character 'c'.
   // Remove directory part -- everything until the last '/'.

   Ssiz_t p = s.Last(c);
   if (p != kNPOS)
      s.Remove(p);

   Ssiz_t ls = s.Last('/');
   if (ls != kNPOS)
      s.Remove(0, ls + 1);
}
}

//______________________________________________________________________________
Bool_t EduKitUtils::CheckMacro(const char* mac)
{
   // Checks if macro 'mac' is loaded.

   // Axel's advice; now sth seems slow, using old method below for test.
   // return gROOT->GetInterpreter()->IsLoaded(mac);

   // Previous version expected function with same name and used ROOT's
   // list of global functions.

   TString foo(mac); ChompTailAndDir(foo);
   if (gROOT->GetGlobalFunction(foo.Data(), 0, kFALSE) != 0)
      return kTRUE;
   else
      return (gROOT->GetGlobalFunction(foo.Data(), 0, kTRUE) != 0);
}

//______________________________________________________________________________
void EduKitUtils::AssertMacro(const char* mac)
{
   // Load and execute macro 'mac' if it has not been loaded yet.

   if( CheckMacro(mac) == kFALSE) {
      gROOT->Macro(mac);
   }
}

//______________________________________________________________________________
void EduKitUtils::Macro(const char* mac)
{
   // Execute macro 'mac'. Do not reload the macro.

   if (CheckMacro(mac) == kFALSE) {
      gROOT->LoadMacro(mac);
   }
   TString foo(mac); ChompTailAndDir(foo); foo += "()";
   gROOT->ProcessLine(foo.Data());
}

//______________________________________________________________________________
void EduKitUtils::LoadMacro(const char* mac)
{
   // Makes sure that macro 'mac' is loaded, but do not reload it.

   if (CheckMacro(mac) == kFALSE) {
      gROOT->LoadMacro(mac);
   }
}